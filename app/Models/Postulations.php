<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Providers;
use App\Models\User;

class Postulations extends Model
{
    use HasFactory;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'postulations';
    protected $fillable = [
        'user_id','providers_id','status',
    ];

    public function users(){
        return $this->HasMany(User::class, 'id');
    } 
    public function providers(){
        return $this->HasMany(Providers::class, 'id');
    } 
}
