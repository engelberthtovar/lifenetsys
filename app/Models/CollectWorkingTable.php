<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollectWorkingTable extends Model
{
    use HasFactory;
    protected $table = "collect_working_tables";
    protected $guarded = [];
    protected $filleable = [
        'MesRecaudo',
        'Asesor',
        'CodGrupoFam',
        'NroSolicitud',
        'Modo',
        'Frecuencia',
        'NumCuotaProgramada',
        'Programa',
        'FechaPrimerCobro',
        'MontoPagoMes',
        'CuotasMesPagadas',
        'provider_id',
        'procesado',
        'fecha_importacion'

    ];
}
