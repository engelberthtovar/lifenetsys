<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Providers;
use App\Models\Network;
use App\Models\User;

class ProvidersNetwork extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'providers_networks';
    protected $fillable = [
        'head_user_id', 'head_user_fullname','recipient_user_id', 'recipient_fullname', 'providers_id', 'providers_comercial_name',
    ];

    public function users(){
        return $this->HasMany(User::class, 'id');
    } 
    public function providers(){
        return $this->HasMany(Providers::class, 'id');
    } 
    public function networks(){
        return $this->HasMany(Network::class, 'sender_user_id', 'sender_fullname','receiver_user_id', 'receiver_fullname');
    } 
}
