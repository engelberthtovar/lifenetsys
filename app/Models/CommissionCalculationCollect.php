<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommissionCalculationCollect extends Model
{
    use HasFactory;
    protected $table = "commission_calculation_collects";
    protected $guarded = [];
    protected $filleable = [
        'commissionist_id',
        'seller_id',
        'provider_id',
        'product_id',
        'grupo_familiar',
        'numero_solicitud',
        'monto',
        'fecha_solicitud',
        'fecha_importacion',
        'monto_fijo',
        'monto_porcentual',
        'calculo',

    ];
}
