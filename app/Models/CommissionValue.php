<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommissionValue extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'commission_values';
    protected $guarded = [];
    protected $fillable = [
        'product_id', 'nivel','colocacion', 'recaudo',
    ];

}
