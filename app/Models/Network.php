<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Network extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'networks';
    protected $fillable = [
        'sender_user_id', 'sender_fullname','receiver_user_id', 'receiver_fullname'
    ];

    public function users(){
        return $this->HasMany(User::class, 'id');
    } 

    
}
