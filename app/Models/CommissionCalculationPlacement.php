<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommissionCalculationPlacement extends Model
{
    use HasFactory;
    protected $table = "commission_calculation_placements";
    protected $guarded = [];
    protected $filleable = [
        'seller_id',
        'provider_id',
        'product_id',
        'grupo_familiar',
        'numero_solicitud',
        'monto',
        'fecha_solicitud',
        'fecha_importacion',
        'monto_fijo',
        'monto_porcentual',
        'calculo',


    ];
}
