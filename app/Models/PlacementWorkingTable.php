<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlacementWorkingTable extends Model
{
    use HasFactory;
    protected $table = "placement_working_tables";
    protected $guarded = [];
    protected $filleable = [
        'modalidad',
        'canal',
        'grupo_familiar',
        'codigo_unico_afiliado',
        'codigo_persona',
        'apellidos_nombres',
        'fecha_nacimiento',
        'sexo',
        'tipo_documento_identidad',
        'numero_documento',
        'codigo_ubigeo',
        'distrito',
        'provincia',
        'departamento',
        'fecha_solicitud',
        'fecha_primer_pago',
        'fecha_liq_recp',
        'fecha_afiliacion',
        'fecha_inicio_vigencia',
        'fecha_fin_vigencia',
        'fecha_continuidad',
        'fecha_baja',
        'programa',
        'rango_etario',
        'tarifa',
        'descripcion_tarifa',
        'fumador',
        'frecuencia',
        'cuota',
        'monto_soles',
        'monto_dolares',
        'tipo_cambio',
        'asesora_sede',
        'descripcion_asesora',
        'tipo_asesora',
        'nombre_coordinador',
        'nombre_supervisor',
        'grupo_canal',
        'numero_afiliados',
        'observacion',
        'tipo_tarjeta',
        'fecha_registro',
        'nro_solicitud',
        'origen_venta',
        'provider_id',
        'procesado',
        'fecha_importacion'

    ];
}
