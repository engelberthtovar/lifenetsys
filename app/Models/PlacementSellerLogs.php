<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlacementSellerLogs extends Model
{
    use HasFactory;
    protected $table = "placement_seller_logs";
    protected $guarded = [];
    protected $filleable = [
        'numero_solicitud',
        'grupo_familiar',
        'codigo_persona',
        'monto_soles',
        'fecha_solicitud',
        'seller_id',
        'provider_id',
        'product_code',
        'procesado'


    ];
}
