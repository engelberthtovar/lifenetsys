<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ColletSellerLogs extends Model
{
    use HasFactory;
    protected $table = "collet_seller_logs";
    protected $guarded = [];
    protected $filleable = [
        'Asesor',
        'CodGrupoFam',
        'NroSolicitud',
        'FechaPrimerCobro',
        'MontoPagoMes',
        'seller_id',
        'provider_id',
        'procesado',

    ];
}
