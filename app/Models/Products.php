<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products';
    protected $fillable = [
            'provider_id',
            'product_name',
            'description',
            'product_code',
            'referencial_value',
            'colocacion',
            'recaudo',
            'calculo_colocacion',
            'calculo_recaudo',
    ];
}
