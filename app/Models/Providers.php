<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    use HasFactory;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'providers';
    protected $fillable = [
        'social_reason',
        'comercial_name',
        'ruc',
        'direction',
        'email',
        'network_size',
        'level_size',
        'size_depth',
        'administrative_contact_name',
        'administrative_contact_phone',
        'status',
    ];


}
