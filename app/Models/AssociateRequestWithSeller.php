<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssociateRequestWithSeller extends Model
{
    use HasFactory;

            /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'associate_request_with_sellers';
    protected $fillable = [
        'provider_id',
        'seller_id',
        'application_number',
        'signature_date',
        'application_registration_date',
        'application_approval_date',
        'client_name',
        'client_surnames',
        'procesado'
 
    ];
}
