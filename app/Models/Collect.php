<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\Importable;

class Collect extends Model
{
    use HasFactory;
    use Importable;
    protected $table = "collect_weft";
    protected $guarded = [];
    protected $filleable = [
        'MesRecaudo',
        'Asesor',
        'CodGrupoFam',
        'NroSolicitud',
        'Modo',
        'Frecuencia',
        'NumCuotaProgramada',
        'Programa',
        'FechaPrimerCobro',
        'MontoPagoMes',
        'CuotasMesPagadas',
        'migrado',
        'provider_id',
        'migrado',
        'fecha_importacion'

    ];
}
