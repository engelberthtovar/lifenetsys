<?php

namespace App\Actions\Fortify;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Network;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'string', 'max:20'],
            'ruc' => ['required', 'string', 'max:20'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();
        DB::transaction(function () use($input){
            $date = Carbon::now();
            $User = User::create([
                'name' => $input['name'],
                'dni' => $input['dni'],
                'ruc' => $input['ruc'],
                'email' => $input['email'],
                'email_verified_at' => $date,
                'code' => uniqid(),
                'password' => Hash::make($input['password']),
            ]); 
            //dd($User->id);
        $UserName = Auth::user()->name;
        Network::create([   
                'sender_user_id' =>  1,
                'receiver_user_id' =>  $User->id,
                'sender_fullname' => $UserName,
                'receiver_fullname' => $User->name,
        
            ]);
        $User->assignRole('view_users');
            return $User;
        });
    }
}

