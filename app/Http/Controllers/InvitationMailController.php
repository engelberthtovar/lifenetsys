<?php

namespace App\Http\Controllers;

use App\Models\Invitation;
use App\Mail\InvitationMail;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller;
use Laravel\Jetstream\Jetstream;

//----Qr Code --//
/*use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use SimpleSoftwareIO\QrCode\Facade as QrCode;*/


class InvitationMailController extends Controller
{
    public function index() 
    {
        return view('profile.invitation');
    }

    public function store(Request $request) 
    { 
        //dump($request->email);
        //dd($request->session('id'));
        //dd($request->user()->getKey());
        $input = $request->email;
        $session = $request->user()->getKey();
        $user = DB::table('users')->where('id',$session)->get();
        $email = $user['0']->email;
        $name = $user['0']->name;
        $code = $user['0']->code;
        //phpinfo();
        //die();
        //$qr = QrCode::format('png')->size(200)->generate('http://127.0.0.1:8000/register');
        
        $data = 
        [
        'name' => $name,
        'sender_email' => $email,
        'receiver_email' => $input,
        'code' => $code,
        'url'   => 'http://127.0.0.1:8000/invitation/store/'.$code.'',
        //'qr' => $qr,
        ];
        //dd($data);
        //dd($user['0']);
        $invitation = Invitation::create([
            'id_sender' => $session,
            'sender_email' => $email,
            'receiver_email' => $input,
            ]);
        //dd($user['0']);

        $send = $this->sendInvitation($data);
        return redirect()->route('dashboard');

    }

    public function sendInvitation($data)
    {
        //dd($data);
        $email = $data['receiver_email'];
        Mail::to($email)->queue(new InvitationMail($data));
        return view('emails.sussesfullinvitation');
    }
 
}

