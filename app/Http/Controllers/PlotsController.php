<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Collect;
use App\Models\Products;
use App\Models\Placement;
use App\Models\Providers;
use Illuminate\Http\Request;
use App\Imports\CollectImport;
use App\Models\CommissionValue;
use App\Imports\PlacementImport;
use App\Models\ProvidersNetwork;
use App\Models\ColletSellerLogs;
use App\Models\PlacementSellerLogs;
use App\Models\CollectWorkingTable;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\PlacementWorkingTable;
use App\Models\AssociateRequestWithSeller;
use App\Models\CommissionCalculationCollect;
use App\Models\CommissionCalculationPlacement;
use App\Models\AssociateRequestWithSellerCollect;

use function PHPUnit\Framework\isEmpty;

class PlotsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPlacement()
    {
        return view('content.import_placement');
    }


        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCollections()
    {
        return view('content.import_collections');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function MigratePlacement()
    {
        $Placements = Placement::all();
        if(($Placements)->isNotEmpty()) {
            $LastRecord = $Placements->last()->id;
            $date = Carbon::now();
            $date = $date->format('d-m-Y');            
            //$PlacementWorkingTable = 0;
            foreach($Placements as $Placement){

             $PlacementWorkingTable = PlacementWorkingTable::create([ 
                "modalidad" => $Placement->modalidad,
                "canal" => $Placement->canal,
                "grupo_familiar" => $Placement->grupo_familiar,
                "codigo_unico_afiliado" => $Placement->codigo_unico_afiliado,
                "codigo_persona" => $Placement->codigo_persona,
                "apellidos_nombres" =>$Placement->apellidos_nombres,
                "fecha_nacimiento" => $Placement->fecha_nacimiento,
                "sexo" => $Placement->sexo,
                "tipo_documento_identidad" => $Placement->tipo_documento_identidad,
                "numero_documento" => $Placement->numero_documento,
                "codigo_ubigeo" => $Placement->codigo_ubigeo,
                "distrito" => $Placement->distrito,
                "provincia" => $Placement->provincia,
                "departamento" => $Placement->departamento,
                "fecha_solicitud" => $Placement->fecha_solicitud,
                "fecha_primer_pago" => $Placement->fecha_primer_pago,
                "fecha_liq_recp" => $Placement->fecha_liq_recp,
                "fecha_afiliacion" => $Placement->fecha_afiliacion,
                "fecha_inicio_vigencia" => $Placement->fecha_inicio_vigencia,
                "fecha_fin_vigencia" => $Placement->fecha_fin_vigencia,
                "fecha_continuidad" => $Placement->fecha_continuidad,
                "fecha_baja" => $Placement->fecha_baja,
                "programa" => $Placement->programa,
                "rango_etario" => $Placement->rango_etario,
                "tarifa" => $Placement->tarifa,
                "descripcion_tarifa" => $Placement->descripcion_tarifa,
                "fumador" => $Placement->fumador,
                "frecuencia" => $Placement->frecuencia,
                "cuota" => $Placement->cuota,
                "monto_soles" => $Placement->monto_soles,
                "monto_dolares" => $Placement->monto_dolares,
                "tipo_cambio" => $Placement->tipo_cambio,
                "asesora_sede" => $Placement->asesora_sede,
                "descripcion_asesora" => $Placement->descripcion_asesora,
                "tipo_asesora" => $Placement->tipo_asesora,
                "nombre_coordinador" => $Placement->nombre_coordinador,
                "nombre_supervisor" => $Placement->nombre_supervisor,
                "grupo_canal" => $Placement->grupo_canal,
                "numero_afiliados" => $Placement->numero_afiliados,
                "observacion" => $Placement->observacion,
                "tipo_tarjeta" => $Placement->tipo_tarjeta,
                "fecha_registro" => $Placement->fecha_registro,
                "nro_solicitud" => $Placement->nro_solicitud,
                "origen_venta" => $Placement->origen_venta,
                "provider_id" => $Placement->provider_id,
                "procesado" => 0,
                "fecha_importacion" => $date,

                ]);
                for ($i=1; $i<=$LastRecord; $i++){

                    Placement::where('id',$i)->update(['migrado' => true]);
                }

            }
                //return back()->with('success','importación Completa');
                return $PlacementWorkingTable;
        }else{
            return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        } 
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function MigrateCollections()
    {
        $Collects = Collect::all();
        if(($Collects)->isNotEmpty()) {
            $LastRecord = $Collects->last()->id;
            $date = Carbon::now();
            $date = $date->format('d-m-Y');
            //dd($dates);
            //$PlacementWorkingTable = new PlacementWorkingTable;
            foreach($Collects as $Collect){

                //dd($Collect);
                CollectWorkingTable::create([ 
                    'MesRecaudo'            => $Collect->MesRecaudo,
                    'Asesor'                => $Collect->Asesor,
                    'CodGrupoFam'           => $Collect->CodGrupoFam,
                    'NroSolicitud'          => $Collect->NroSolicitud,
                    'Modo'                  => $Collect->Modo,
                    'Frecuencia'            => $Collect->Frecuencia,
                    'NumCuotaProgramada'    => $Collect->NumCuotaProgramada,
                    'Programa'              => $Collect->Programa,
                    'FechaPrimerCobro'      => $Collect->FechaPrimerCobro,
                    'MontoPagoMes'          => $Collect->MontoPagoMes,
                    'CuotasMesPagadas'      => $Collect->CuotasMesPagadas,
                    'provider_id'           => $Collect->provider_id,
                    "fecha_importacion"     => $date,

    
                ]);
                for ($i=1; $i<=$LastRecord; $i++){

                    $Record = Collect::where('id',$i)->update(['migrado' => true]);
                }

            }
            return back()->with('success','importación Completa');
        }else{
            return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        }    
        //return $PlacementWorkingTable;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePlacement(Request $request)
    {
        $file = $request->file('file');
        Excel::import(new PlacementImport, $request->file);

       $this->MigratePlacement();

        return back()->with('succes','importación Completa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCollections(Request $request)
    {
        $file = $request->file('file');
        Excel::import(new CollectImport, $request->file);

        $this->MigrateCollections();

        return back()->with('success','Importación Completa');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showPlacementContracts()
    {
        $Bucles = PlacementWorkingTable::where('procesado', 0)->get();
        $BucleLast = $Bucles->last();
        $Buclefirst = $Bucles->first();
        //dd($Bucles);
        if($Buclefirst !== null){
            for ($i=$Buclefirst->id; $i <= $BucleLast->id; $i++){
                $PlacementWorkingTable = PlacementWorkingTable::where('procesado', 0)->first();
                if($PlacementWorkingTable !== null){
                    //dd($PlacementWorkingTable);
                    $Seller = $this->FindPlacementSeller($PlacementWorkingTable->nro_solicitud);
                        if($Seller !== null){
                            foreach($Seller as $Sellers){
                                $SellerId = $Sellers->seller_id;
                                $ProviderId = $Sellers->provider_id;
                            }
                            $PlacementsData = [
                                'numero_solicitud'   => $PlacementWorkingTable->nro_solicitud,
                                'grupo_familiar'     => $PlacementWorkingTable->grupo_familiar,
                                'codigo_persona'     => $PlacementWorkingTable->codigo_persona,
                                'monto_soles'        => $PlacementWorkingTable->monto_soles,
                                'fecha_solicitud'    => $PlacementWorkingTable->fecha_solicitud,
                                'fecha_liq_recp'     => $PlacementWorkingTable->fecha_liq_recp,
                                'seller_id'          => $SellerId,
                                'provider_id'        => $ProviderId,
                                'product_code'       => $PlacementWorkingTable->programa,
                                'procesado'          => 0,
                            ];
                            $this->storePlacementSellerLog($PlacementsData);                            
                            PlacementWorkingTable::where('id',$PlacementWorkingTable->id)->update(['procesado' => 2]);
                            $this-> CommissionCalculationPlacement();
                            if($BucleLast->id === $i){
                                return redirect()->route('dashboard')->with('success','Comisiones Procesadas Con Exito');
                            }
                        }else{
                            $PlacementsData = [
                                'numero_solicitud'   => $PlacementWorkingTable->nro_solicitud,
                                'grupo_familiar'     => $PlacementWorkingTable->grupo_familiar,
                                'codigo_persona'     => $PlacementWorkingTable->codigo_persona,
                                'monto_soles'        => $PlacementWorkingTable->monto_soles,
                                'fecha_solicitud'    => $PlacementWorkingTable->fecha_solicitud,
                                'seller_id'          => $Seller,
                                'procesado'          => 1,

                            ];
                            PlacementWorkingTable::where('id',$PlacementWorkingTable->id)->update(['procesado' => 1]);
                            if($BucleLast->id === $i){
                                return redirect()->route('dashboard')->with('success','Comisiones Procesadas Con Exito');
                            }
                        }
                }else{
                    return redirect()->route('dashboard')->with('success','No hay mas Registros en la Tabla Colocacion'); 
                }    
            }
            
        }else{
            return redirect()->route('dashboard')->with('success','No hay mas Registros en la Tabla Colocacion'); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storePlacementSellerLog($param)
    {
        $PlacementSellerLogs = new PlacementSellerLogs;
        $PlacementSellerLogs->create($param);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function FindPlacementSeller($id)
    {
  
        $AssociateRequestWithSeller = AssociateRequestWithSeller::where('application_number', $id)->get();
 
        if(!$AssociateRequestWithSeller->isEmpty()){

            return $AssociateRequestWithSeller;
        }else{
            $AssociateRequestWithSeller = null;

            return $AssociateRequestWithSeller;
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCollectContracts()
    {
        $Bucles = CollectWorkingTable::where('procesado', 0)->get();
        $Buclefirst = $Bucles->first();
        $BucleLast = $Bucles->last();
        //dd($Bucles);
        if($Buclefirst !== null){
            for ($i=$Buclefirst->id; $i <= $BucleLast->id; $i++){
                $CollectWorkingTable = CollectWorkingTable::where('procesado', 0)->first();
                //dd($CollectWorkingTable->Programa);
                if($CollectWorkingTable !== null){
                    $Seller = $this->FindCollectSeller($CollectWorkingTable->NroSolicitud);
                        //dd($Seller);
                        if($Seller !== null){
                            foreach($Seller as $Sellers){
                                $SellerId = $Sellers->seller_id;
                                $ProviderId = $Sellers->provider_id;
                            }
                            $CollectsData = [
                                'Asesor'              => $CollectWorkingTable->Asesor,
                                'CodGrupoFam'         => $CollectWorkingTable->CodGrupoFam,
                                'NroSolicitud'        => $CollectWorkingTable->NroSolicitud,
                                'MesRecaudo'          => $CollectWorkingTable->MesRecaudo,
                                'FechaPrimerCobro'    => $CollectWorkingTable->FechaPrimerCobro,
                                'NumCuotaProgramada'  => $CollectWorkingTable->NumCuotaProgramada,
                                'MontoPagoMes'        => $CollectWorkingTable->MontoPagoMes,
                                'seller_id'           => $SellerId,
                                'provider_id'         => $ProviderId,
                                'product_code'        => $CollectWorkingTable->Programa,
                                'procesado'           => 0,
                            ];
                            //dd($CollectsData);
                            $this->storeCollectSellerLog($CollectsData);                           
                            CollectWorkingTable::where('id',$CollectWorkingTable->id)->update(['procesado' => 2]);
                            $this->CommissionCalculationCollect();
                            if($BucleLast->id === $i){
                                return redirect()->route('dashboard')->with('success','Comisiones Procesadas Con Exito');
                            }
                            
                            //return redirect()->route('dashboard')->with('success','Comisiones Procesadas Con Exito'); 
                            //dd($CollectsData);
                        }else{
                            $CollectsData = [
                                'Asesor'            => $CollectWorkingTable->Asesor,
                                'CodGrupoFam'       => $CollectWorkingTable->CodGrupoFam,
                                'NroSolicitud'      => $CollectWorkingTable->NroSolicitud,
                                'FechaPrimerCobro'  => $CollectWorkingTable->FechaPrimerCobro,
                                'MontoPagoMes'      => $CollectWorkingTable->MontoPagoMes,
                                'seller_id'         => $Seller,

                            ];
                            //dd($BucleLast->id);
                            CollectWorkingTable::where('id',$CollectWorkingTable->id)->update(['procesado' => 1]);
                            if($BucleLast->id === $i){
                                return redirect()->route('dashboard')->with('success','Comisiones Procesadas Con Exito');
                            }
                            //dd($CollectsData);
                            //return redirect()->route('dashboard')->with('warning','Algunos Procesos quedaron Pendientes'); 
                        }
                }else{
                    return redirect()->route('dashboard')->with('success','No hay mas Registros en la Tabla Recaudo'); 
                }    
            }
        }else{
            return redirect()->route('dashboard')->with('success','No hay mas Registros en la Tabla Recaudo'); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeCollectSellerLog($param)
    {   
        $ColletSellerLogs = new ColletSellerLogs;
        $ColletSellerLogs->create($param);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function FindCollectSeller($id)
    {
  
        $AssociateRequestWithSeller = AssociateRequestWithSellerCollect::where('application_number', $id)->get();
        //dd($AssociateRequestWithSeller);
 
        if(!$AssociateRequestWithSeller->isEmpty()){

            return $AssociateRequestWithSeller;
        }else{
            $AssociateRequestWithSeller = null;

            return $AssociateRequestWithSeller;
        }
    }



    public function CommissionCalculationCollect()
    {
        $AssociateRequestWithSellerCollect = AssociateRequestWithSellerCollect::where('procesado', 0)->get();
        $ColletSellerLogs = ColletSellerLogs::where('procesado', 0)->get();
        //dd($ColletSellerLogs);
        $Buclefirst = $ColletSellerLogs->first();
        $BucleLast = $ColletSellerLogs->last();
        if($Buclefirst !== null){
            for ($i=$Buclefirst->id; $i <= $BucleLast->id; $i++){
                $ColletSellerLogs = ColletSellerLogs::where('procesado', 0)->get();
                $ColletSellerLogsfirst = $ColletSellerLogs->first();
                //dd($ColletSellerLogsfirst);
                if($ColletSellerLogs->isNotEmpty()) {
                    $Products = Products::where('product_name', $ColletSellerLogsfirst->product_code)
                                        ->where('recaudo', 'SI')->get();
                    $Product = $Products->first();
                    
                    if($Product !== null) {           
                        $ProductAmountP = 0;
                        $ProductAmountF = 0;
                        switch ($Product->recaudo){
                            case "SI":
                            $Type = $Product->calculo_recaudo;
                            $Commission = 'recaudo';
                            break;
                       
                        }
                        
                        switch ($Commission){

                            case "recaudo":
                                if( $Type === "%"){

                                    $CommissionValue = CommissionValue::where('product_id', $Product->id)->orderBy('nivel', 'ASC')->get();
                                    $CommissionS = $CommissionValue->first(); 
                                    $ProductAmountF = $CommissionS->colocacion;
                                    $ProductAmountP = $CommissionS->recaudo;
                                    $Calculo = ($ProductAmountF) + (($ProductAmountP/100) * $ColletSellerLogsfirst->MontoPagoMes);
                                    
                                }else{

                                    $CommissionValue = CommissionValue::where('product_id', $Product->id)->orderBy('nivel', 'ASC')->get();
                                    $CommissionS = $CommissionValue->first();
                                    $ProductAmountF = $CommissionS->recaudo;
                                    $ProductAmountP = $CommissionS->colocacion;
                                    $Calculo = ($ProductAmountF) + (($ProductAmountP/100) * $ColletSellerLogsfirst->MontoPagoMes);
                                }
                            break;
                        }                   
                        $CommissionData20 = [
                            'commissionist_id'      => $ColletSellerLogsfirst->seller_id,
                            'seller_id'             => $ColletSellerLogsfirst->seller_id,
                            'provider_id'           => $ColletSellerLogsfirst->provider_id,
                            'product_id'            => $Product->id,
                            'grupo_familiar'        => $ColletSellerLogsfirst->CodGrupoFam,
                            'numero_solicitud'      => $ColletSellerLogsfirst->NroSolicitud,
                            'num_cuota_programada'  => $ColletSellerLogsfirst->NumCuotaProgramada,
                            'fecha_importacion'     => $ColletSellerLogsfirst->created_at,
                            'mes_recaudo'           => $ColletSellerLogsfirst->MesRecaudo,
                            'monto'                 => $ColletSellerLogsfirst->MontoPagoMes,
                            'monto_fijo'            => $ProductAmountF,
                            'monto_porcentual'      => $ProductAmountP,
                            'calculo'               => $Calculo,
                            //'fecha_solicitud'       => $ColletSellerLogsfirst->fecha_solicitud,

                        ];
                        $CommissionCalculationCollect = new CommissionCalculationCollect;
                        $CommissionCalculationCollect->create($CommissionData20);

                        $Providers = Providers::where('id',$ColletSellerLogsfirst->provider_id )->get();

                        $Providerlevels = $Providers['0']->size_depth;
                        $ProvidersNetwork = ProvidersNetwork::where('recipient_user_id', $ColletSellerLogsfirst->seller_id)->get();

                        $TempComissionist = $ColletSellerLogsfirst->seller_id;
                        $TempProvider     = $ColletSellerLogsfirst->provider_id;
                        $CommissionArray = [
                            'level_1'         => $TempComissionist,

                        ];
                        $ProductAmountsP = 0;
                        $ProductAmountsF = 0;
                        for($i = 2; $i <= $Providerlevels; $i++){
                            $ProvidersNetworks = ProvidersNetwork::where('recipient_user_id', $TempComissionist)
                                                            ->where('providers_id',$TempProvider)->get();
                            foreach($ProvidersNetworks as $ProvidersNetwork){
                                $ProvidersNetworkHeadId = $ProvidersNetwork->head_user_id;
                            }
                        
                            $CommissionV = CommissionValue::where('product_id', $Product->id)
                                                            ->where('nivel', $i)
                                                            ->orderBy('nivel', 'ASC')->get();
                            $CommissionSv = $CommissionV->first();
                            //dd($CommissionSv);
                            if($CommissionSv->recaudo !== "0" && $Type === "%"){

                                $ProductAmountsF = $CommissionSv->colocacion;
                                $ProductAmountsP = $CommissionSv->recaudo;
                                $Calculos = ($ProductAmountsF) + (($ProductAmountsP/100) *  $ColletSellerLogsfirst->MontoPagoMes);

                            }else if ($CommissionSv->recaudo !== "0" && $Type !== "%" ){

                                $ProductAmountsP = $CommissionSv->colocacion;
                                $ProductAmountsF = $CommissionSv->recaudo;
                                $Calculos = ($ProductAmountsF) + (($ProductAmountsP/100) *  $ColletSellerLogsfirst->MontoPagoMes);


                            }                         
                            if(isEmpty($ProvidersNetworks) === true) {

                            $ProvidersNetworkHeadId = $TempComissionist;

                            }

                            $HeadUser  = $ProvidersNetworkHeadId;
                            if($TempComissionist === $HeadUser ){

                                break;

                            }   
                            $key = 'level_'.$i;
                            array_push($CommissionArray, $HeadUser);
                            $CommissionArray[$key] = $HeadUser;
                            $TempComissionist = $HeadUser;
                            $CommissionData = [
                                'commissionist_id'      => $CommissionArray[$key],
                                'seller_id'             => $ColletSellerLogsfirst->seller_id,
                                'provider_id'           => $ColletSellerLogsfirst->provider_id,
                                'product_id'            => $Product->id,
                                'grupo_familiar'        => $ColletSellerLogsfirst->CodGrupoFam,
                                'numero_solicitud'      => $ColletSellerLogsfirst->NroSolicitud,
                                'num_cuota_programada'  => $ColletSellerLogsfirst->NumCuotaProgramada,
                                'fecha_importacion'     => $ColletSellerLogsfirst->created_at,
                                'mes_recaudo'           => $ColletSellerLogsfirst->MesRecaudo,
                                'monto'                 => $ColletSellerLogsfirst->MontoPagoMes,
                                'monto_fijo'            => $ProductAmountsF,
                                'monto_porcentual'      => $ProductAmountsP,
                                'calculo'               => $Calculos,
                                //'fecha_solicitud'       => $ColletSellerLogsfirst->fecha_solicitud,
                            ];
                            ColletSellerLogs::where('id',$ColletSellerLogsfirst->id)->update(['procesado' => 1]);
                            $CommissionCalculationCollect = new CommissionCalculationCollect;
                            $CommissionCalculationCollect->create($CommissionData);
                            
                        } 
                    }else{
                        return redirect()->route('dashboard')->with('errors','No Hay Ningun Producto con Este Nombre');
                    }     
                }else{
                    return redirect()->route('dashboard')->with('success','No hay mas Registros'); 
                }       
                    
            }
        }else{
            return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        }
        
                            
        return redirect()->route('dashboard')->with('success','No hay mas Registros'); 
 
    }







    public function funcioncalculo()
    {
        $cantidad_de_filas = CommissionCalculationPlacement::where ('procesado',1)->get();
        $cantidad_de_fila = $cantidad_de_filas->last();
 
        for ($i = 0; $i < $cantidad_de_fila->id; $i++)
        {
            $Comisione = CommissionCalculationPlacement::where('provider_id',1)
                                                        ->where ('procesado',1)->get();
            $Comisiones = $Comisione->first();
            $monto_fijo = $Comisiones->monto_fijo;
            $monto_porcentual = $Comisiones->monto_porcentual;
            $monto = $Comisiones->monto;
        
            $calculo = ($monto_fijo) + (($monto_porcentual/100) * $monto);
        
            CommissionCalculationPlacement::where('id',$Comisiones->id)->update(['calculo' => $calculo]);
            
           CommissionCalculationPlacement::where('id',$Comisiones->id)->update(['procesado' => 2]);

        
        }
        return redirect()->route('dashboard')->with('success','No hay mas Registros'); 
    }




    public function CommissionCalculationPlacement()
    {

        //$AssociateRequestWithSeller = AssociateRequestWithSeller::where('procesado', 0)->get();
        $PlacementLogs = PlacementSellerLogs::where('procesado', 0)->get();
        //dd($PlacementLogs);
        $Buclefirst = $PlacementLogs->first();
        $BucleLast = $PlacementLogs->last();
        if($Buclefirst !== null){
            for ($i=$Buclefirst->id; $i <= $BucleLast->id; $i++){
                $PlacementSellerLogs = PlacementSellerLogs::where('procesado', 0)->get();
                $PlacementSellerLogsfirst = $PlacementSellerLogs->first();
                if($PlacementSellerLogs->isNotEmpty()) {
                    $Products = Products::where('product_name', $PlacementSellerLogsfirst->product_code)
                                        ->where('colocacion', 'SI')->get();
                    $Product = $Products->first();
                    //dd($PlacementSellerLogsfirst);
                    if($Product !== null) {            
                        $ProductAmountP = 0;
                        $ProductAmountF = 0;
                        switch ($Product->colocacion){
                            case "SI":
                            $Type = $Product->calculo_colocacion;
                            $Commission = 'colocaion';
                            break;

                            case "NO":
                                $Type =  $Product->calculo_recaudo;
                                $Commission = 'recaudo';
                            break;
        
                        }
                        
                        switch ($Commission){
                           
                            case "colocaion":
                                if( $Type === "%"){

                                    $CommissionValue = CommissionValue::where('product_id', $Product->id)->orderBy('nivel', 'ASC')->get();
                                    $CommissionS = $CommissionValue->first();
                                    //dd($CommissionS);
                                    $ProductAmountP = $CommissionS->colocacion;
                                    $ProductAmountF = $CommissionS->recaudo;
                                    $Calculo = $ProductAmountF + $PlacementSellerLogsfirst->monto_soles/100 * $ProductAmountP;

                                }else{

                                    $CommissionValue = CommissionValue::where('product_id', $Product->id)->orderBy('nivel', 'ASC')->get();
                                    $CommissionS = $CommissionValue->first();
                                    
                                    $ProductAmountF = $CommissionS->colocacion;
                                    $ProductAmountP =$CommissionS->recaudo;
                                    $Calculo = $ProductAmountF + $PlacementSellerLogsfirst->monto_soles/100 * $ProductAmountP;
                                    
                                }
                            break;

                            case "recaudo":
                                if( $Type === "%"){

                                    $CommissionValue = CommissionValue::where('product_id', $Product->id)->orderBy('nivel', 'ASC')->get();
                                    $CommissionS = $CommissionValue->first();
                                    $ProductAmountP = $CommissionS->recaudo;
                                    $ProductAmountF = $CommissionS->colocacion;
                                    $Calculo = $ProductAmountF + $PlacementSellerLogsfirst->monto_soles/100 * $ProductAmountP;
                                    
                                }else{

                                    $CommissionValue = CommissionValue::where('product_id', $Product->id)->orderBy('nivel', 'ASC')->get();
                                    $CommissionS = $CommissionValue->first();
                                    $ProductAmountP = $CommissionS->colocacion;
                                    $ProductAmountF = $CommissionS->recaudo;
                                    $Calculo = $ProductAmountF + $PlacementSellerLogsfirst->monto_soles/100 * $ProductAmountP;
                                }
                            break;
                        }  
                                         
                        $CommissionData20 = [
                            'commissionist_id'      => $PlacementSellerLogsfirst->seller_id,
                            'seller_id'             => $PlacementSellerLogsfirst->seller_id,
                            'provider_id'           => $PlacementSellerLogsfirst->provider_id,
                            'product_id'            => $Product->id,
                            'grupo_familiar'        => $PlacementSellerLogsfirst->grupo_familiar,
                            'numero_solicitud'      => $PlacementSellerLogsfirst->numero_solicitud,
                            'fecha_liq_recp'        => $PlacementSellerLogsfirst->fecha_liq_recp,
                            'fecha_importacion'     => $PlacementSellerLogsfirst->created_at,
                            'monto'                 => $PlacementSellerLogsfirst->monto_soles,
                            'monto_fijo'            => $ProductAmountF,
                            'monto_porcentual'      => $ProductAmountP,
                            'calculo'               => $Calculo,
                            'fecha_solicitud'       => $PlacementSellerLogsfirst->fecha_solicitud,
                            'procesado' => 2
                        ];

                        $CommissionCalculationPlacement = new CommissionCalculationPlacement;
                        $CommissionCalculationPlacement->create($CommissionData20);

                        $Providers = Providers::where('id',$PlacementSellerLogsfirst->provider_id )->get();

                        $Providerlevels = $Providers['0']->size_depth;
                        $ProvidersNetwork = ProvidersNetwork::where('recipient_user_id', $PlacementSellerLogsfirst->seller_id)->get();

                        $TempComissionist = $PlacementSellerLogsfirst->seller_id;
                        $TempProvider     = $PlacementSellerLogsfirst->provider_id;
                        $CommissionArray = [
                            'level_1'         => $TempComissionist,

                        ];
                        $ProductAmountsP = 0;
                        $ProductAmountsF = 0;
                        for($i = 2; $i <= $Providerlevels; $i++){
                            $ProvidersNetworks = ProvidersNetwork::where('recipient_user_id', $TempComissionist)
                                                                ->where('providers_id',$TempProvider)->get();
                            foreach($ProvidersNetworks as $ProvidersNetwork){
                                $ProvidersNetworkHeadId = $ProvidersNetwork->head_user_id;
                            }
                        
                            $CommissionV = CommissionValue::where('product_id', $Product->id)
                                                            ->where('nivel', $i)
                                                            ->orderBy('nivel', 'ASC')->get();
                            $CommissionSv = $CommissionV->first();
                            //dd($CommissionSv);
                            if($CommissionSv->colocacion !== "0" && $Type === "%" ){    //colocacion

                            
                                //$calculo = ($monto_fijo) + (($monto_porcentual/100) * $monto);

                                $ProductAmountsP = $CommissionSv->colocacion;
                                $ProductAmountsF = $CommissionSv->recaudo;
                                $Calculos = ($ProductAmountsF) + (($ProductAmountsP/100) * $PlacementSellerLogsfirst->monto_soles);
                                //dd($Calculos);

                            }else if ($CommissionSv->colocacion !== "0" && $Type !== "%" ){

                                $ProductAmountsF = $CommissionSv->colocacion;
                                //dd($ProductAmountsF);
                                $ProductAmountsP = $CommissionSv->recaudo;
                                $ProductAmountsF = $CommissionSv->colocacion;
                                $Calculos = ($ProductAmountsF) + (($ProductAmountsP/100) * $PlacementSellerLogsfirst->monto_soles);
                                //$Calculos = $ProductAmountF;

                            }

                            /*if($CommissionSv->recaudo !== "No Posee" && $Type === "%"){


                                $ProductAmountsP = $CommissionSv->recaudo;

                                $Calculos = $PlacementSellerLogsfirst->monto_soles/100 * $ProductAmountsP;
                                //dd($CommissionSv->recaudo);

                            }else if ($CommissionSv->recaudo !== "No Posee" && $Type !== "%" ){


                                $ProductAmountsF = $CommissionSv->recaudo;

                                $Calculos = $PlacementSellerLogsfirst->monto_soles + $ProductAmountsF;

                                //dd($CommissionSv->recaudo);

                            }*/
                            //dd($ProductAmounts);
                            //dd($ProvidersNetworks);
                            
                            if(isEmpty($ProvidersNetworks) === true) {
                            $ProvidersNetworkHeadId = $TempComissionist;

                            }
                            $HeadUser  = $ProvidersNetworkHeadId;

                            if($TempComissionist === $HeadUser ){

                                break;

                            }   
                            $key = 'level_'.$i;
                            array_push($CommissionArray, $HeadUser);
                            $CommissionArray[$key] = $HeadUser;
                            $TempComissionist = $HeadUser;
                            $CommissionData = [
                                'commissionist_id'      => $CommissionArray[$key],
                                'seller_id'             => $PlacementSellerLogsfirst->seller_id,
                                'provider_id'           => $PlacementSellerLogsfirst->provider_id,
                                'product_id'            => $Product->id,
                                'grupo_familiar'        => $PlacementSellerLogsfirst->grupo_familiar,
                                'numero_solicitud'      => $PlacementSellerLogsfirst->numero_solicitud,
                                'fecha_importacion'     => $PlacementSellerLogsfirst->created_at,
                                'fecha_liq_recp'        => $PlacementSellerLogsfirst->fecha_liq_recp,
                                'monto'                 => $PlacementSellerLogsfirst->monto_soles,
                                'monto_fijo'            => $ProductAmountsF,
                                'monto_porcentual'      => $ProductAmountsP,
                                'calculo'               => $Calculos,
                                'fecha_solicitud'       => $PlacementSellerLogsfirst->fecha_solicitud,
                                'procesado' => 2
                            ];
                            PlacementSellerLogs::where('id',$PlacementSellerLogsfirst->id)->update(['procesado' => 1]);
                            //CommissionCalculationPlacement::where('id',$PlacementSellerLogsfirst->id)->update(['procesado' => 1]);
                            $CommissionCalculationPlacement = new CommissionCalculationPlacement;
                            $CommissionCalculationPlacement->create($CommissionData);
                        } 
                    }else{
                        return redirect()->route('dashboard')->with('errors','No Hay Ningun Producto con Este Nombre');
                    }

                }else{
                    return redirect()->route('dashboard')->with('success','No hay mas Registros'); 
                }                          
            }
        }else{
                return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        }                      
            return redirect()->route('dashboard')->with('success','No hay mas Registros'); 
            
    }


    public function CommissionsPlacementReport()
    {

        $CommissionCalculationPlacement = CommissionCalculationPlacement::all();
        //dd($CommissionCalculationCollect);
        if(($CommissionCalculationPlacement)->isNotEmpty()) {
            foreach($CommissionCalculationPlacement as $Placement){

                $Commissionist = User::where('id', $Placement->commissionist_id)->get();
                $Commissionists = $Commissionist->first();
                $CommissionistName = $Commissionists->name;
                $Seller = User::where('id', $Placement->seller_id)->get();
                $Sellers = $Seller->first();
                $SellerName = $Sellers->name;
                $Product = Products::where('id',$Placement->product_id );
                $Products = $Product->first();
                $ProductName = $Products->product_name;
                $Providers = Providers::where('id',$Placement->provider_id );
                $Provider = $Providers->first();
                $ProviderName = $Provider->comercial_name;
                //dd($ProviderName);
                $datas = [
                    'commissionist_id'   => $Placement->commissionist_id, 
                    'commissionist_name' => $CommissionistName, 
                    'seller_name'        => $SellerName,
                    'provider_name'      => $ProviderName, 
                    'product_name'       => $ProductName, 
                    'grupo_familiar'     => $Placement->grupo_familiar, 
                    'numero_solicitud'   => $Placement->numero_solicitud, 
                    'monto'              => $Placement->monto, 
                    'fecha_liq_recp'     => $Placement->fecha_liq_recp,
                    'fecha_solicitud'    => $Placement->fecha_solicitud, 
                    'fecha_importacion'  => Carbon::parse($Placement->fecha_importacion)->format('Y-m-d'), 
                    'monto_fijo'         => $Placement->monto_fijo, 
                    'monto_porcentual'   => $Placement->monto_porcentual, 
                    'calculo'            => $Placement->calculo,    
                    ];
                    $xD[] = $datas;
                    $data = $this->elementosUnicos($xD);
            }
                //dd($data);

                return view('content.commissions_placement_report', compact('data'));

        }else{
                return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        }
        

    }

    public function  ConsolidatedCommissionsPlacementReport()
    {
        $CommissionCalculationCollect = CommissionCalculationPlacement::all();
        //dd($CommissionCalculationCollect);
        //$categorias = CommissionCalculationCollect::sum('calculo')->get();
        if(($CommissionCalculationCollect)->isNotEmpty()) {
 
            foreach($CommissionCalculationCollect as $Collect){
                $Commissionist = User::where('id', $Collect->commissionist_id)->get();
                $Commissionists = $Commissionist->first();
                $CommissionistId = $Commissionists->id;
                $CommissionistName = $Commissionists->name;
                $Calculate = CommissionCalculationPlacement::where('commissionist_id', $CommissionistId)->get()->sum('calculo');
                $Count = CommissionCalculationPlacement::where('commissionist_id', $CommissionistId)->get()->count('commissionist_id');
                
                //dd($Count);
                $datas = [
                    'commissionist_id'=> $CommissionistId, 
                    'commissionist_name'=>  $CommissionistName, 
                    'suma'=> $Calculate, 
                    'count' => $Count,
                    ];
                    $xD[] = $datas; 
                    $data = $this->elementosUnicos($xD);
            }
         
            return view('content.consolidated_commissions_placement_report', compact('data'));

        }else{
            return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        }
    }

    public function CommissionsCollectReport()
    {

        $CommissionCalculationCollect = CommissionCalculationCollect::all();
        //dd($CommissionCalculationCollect);
        if(($CommissionCalculationCollect)->isNotEmpty()) {
            foreach($CommissionCalculationCollect as $Collect){

                $Commissionist = User::where('id', $Collect->commissionist_id)->get();
                $Commissionists = $Commissionist->first();
                $CommissionistName = $Commissionists->name;
                $Seller = User::where('id', $Collect->seller_id)->get();
                $Sellers = $Seller->first();
                $SellerName = $Sellers->name;
                $Product = Products::where('id',$Collect->product_id );
                $Products = $Product->first();
                $ProductName = $Products->product_name;
                $Providers = Providers::where('id',$Collect->provider_id );
                $Provider = $Providers->first();
                $ProviderName = $Provider->comercial_name;
                //dd($ProviderName);
                $datas = [
                    'commissionist_id'     => $Collect->commissionist_id, 
                    'commissionist_name'   =>  $CommissionistName, 
                    'seller_name'          =>  $SellerName,
                    'provider_name'        => $ProviderName, 
                    'product_name'         => $ProductName, 
                    'grupo_familiar'       => $Collect->grupo_familiar, 
                    'numero_solicitud'     => $Collect->numero_solicitud, 
                    'monto'                => $Collect->monto, 
                    'mes_recaudo'          => $Collect->mes_recaudo,
                    'num_cuota_programada' => $Collect->num_cuota_programada, 
                    'fecha_importacion'    =>  Carbon::parse($Collect->fecha_importacion)->format('Y-m-d'), 
                    'monto_fijo'           => $Collect->monto_fijo, 
                    'monto_porcentual'     => $Collect->monto_porcentual, 
                    'calculo'              => $Collect->calculo,    
                    ];
                    $xD[] = $datas;
                    $data = $this->elementosUnicos($xD);
                }
                //dd($data);
            return view('content.commissions_collect_report', compact('data'));
            
        }else{
            return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
        }

    }

    public function  ConsolidatedCommissionsCollectReport()
    {
        $CommissionCalculationCollect = CommissionCalculationCollect::all();
        //dd($CommissionCalculationCollect);
        if(($CommissionCalculationCollect)->isNotEmpty()) {
        //$categorias = CommissionCalculationCollect::sum('calculo')->get();
            foreach($CommissionCalculationCollect as $Collect){
                $Commissionist = User::where('id', $Collect->commissionist_id)->get();
                $Commissionists = $Commissionist->first();
                $CommissionistId = $Commissionists->id;
                $CommissionistName = $Commissionists->name;
                $Calculate = CommissionCalculationCollect::where('commissionist_id', $CommissionistId)->get()->sum('calculo');
                $Count = CommissionCalculationCollect::where('commissionist_id', $CommissionistId)->get()->count('commissionist_id');
                
                //dd($Count);
                $datas = [
                    'commissionist_id'=> $CommissionistId, 
                    'commissionist_name'=>  $CommissionistName, 
                    'suma'=> $Calculate, 
                    'count' => $Count,
                    ];
                    $xD[] = $datas;   
                    $data = $this->elementosUnicos($xD);
            }
                return view('content.consolidated_commissions_collect_report', compact('data'));
                
       
        }else{

            return redirect()->route('dashboard')->with('errors','No hay Registros en la Tabla'); 
           
        } 
    }

    function elementosUnicos($array)
    {
        $arraySinDuplicados = [];
        foreach($array as $indice => $elemento) {
            if (!in_array($elemento, $arraySinDuplicados)) {
                $arraySinDuplicados[$indice] = $elemento;
            }
        }
        return $arraySinDuplicados;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sellerLists(Request $request)
    {
        try {    
            
            $application_number = $request->input('application_number');
            $AssociateRequestWithSeller = AssociateRequestWithSeller::where('application_number', $application_number)->get();
            foreach($AssociateRequestWithSeller as $Seller){
                $User = User::where('id',$Seller->seller_id)->get();
                
            }
            //dd($User);
            $response = ['data' => $User];
        } catch (\Exception $exception) {
            return response()->json([ 'message' => 'No hay Usuarios asociados a este Proveedor' ], 500);
        }
        return response()->json($response);
    }
       
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
