<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Providers;
use App\Models\CommissionValue;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $Providers = Providers::all();
        return view('content.add_products', compact('Providers'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //dd($request->monto_colocacion1);
        //$Products = new Products;
        //$Products->create($request->all());
        /*if ($request->calculo_colocacion !== null){
            $calculo_colocacion = $request->calculo_colocacion;
        }else if($request->calculo_colocacion !== '%'){
            $calculo_colocacion = 'FIJO';
        }else if($request->calculo_colocacion === null ){
            $calculo_colocacion = 'No Posee';
        }
        if ($request->calculo_recaudo !== null){
            $calculo_recaudo = $request->calculo_recaudo;
        }else if($request->calculo_recaudo === null){
            $calculo_recaudo = 'FIJO';
        }else if( $request->calculo_recaudo !== '%'){
            $calculo_recaudo = 'No Posee';
        }
        */
      DB::transaction(function () use($request){
         
            Products::create([   
                'provider_id'        => $request->provider_id,
                'product_name'       => $request->product_name,
                'description'        => $request->description,
                'product_code'       => $request->code,
                'referencial_value'  => $request->referencial_value,
                'colocacion'         => $request->monto_colocacion1 !== null ? 'SI' : 'NO',
                'recaudo'            => $request->monto_recaudo1 !== null ? 'SI' : 'NO',
                'calculo_colocacion' => $request->calculo_colocacion === null ? 'No Posee' : $request->calculo_colocacion,
                'calculo_recaudo'    => $request->calculo_recaudo === null ? 'No Posee' : $request->calculo_recaudo,
            ]);
            $Products = Products::where('provider_id', $request->provider_id)->get();
            $Product = $Products->last();

            //dd($Product);

            for ($i = 1; $i <= $request->nivel; $i++) {
                $colocacion = "monto_colocacion".$i;
                $monto_colocacion = $request->$colocacion;
                $recaudo = "monto_recaudo".$i;
                $monto_recaudo = $request->$recaudo;
                //dd($request->all());
                //dd($monto_colocacion);
                
            $CommissionValue = new CommissionValue();
            $CommissionValue =  CommissionValue::create([  
                   'product_id' => $Product->id,
                    'nivel' => $i,
                    'colocacion' => $monto_colocacion === null ? '0' : $monto_colocacion,
                    'recaudo' => $monto_recaudo === null ? '0' : $monto_recaudo,
    
                ]);
            }

        });



        //die;
        return redirect()->back()->with('success', 'El Producto se creo Satisfactoriamente ');
        /*
        $product_id = Input::get('ingredient_names');
        $nivel = Input::get('ingredient_other_field');
        $colocacion = Input::get('ingredient_other_field2');
        $recaudo = Input::get('ingredient_other_field2');
        
        $ingredients = [];
        
        for ($i = 0; $i < count($ingredient_names); $i++) {
            $CommissionValue[] = new  CommissionValue([
                'product_id' => $ingredient_names[$i],
                'nivel' => $ingredient_other_field[$i],
                'colocacion' => $ingredient_other_field2[$i],
                'recaudo' => $ingredient_other_field2[$i],
            ]);
        }
        
        $meal->ingredients()->saveMany($ingredients);
        return redirect()->back();
*/

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $Products = Products::all();
        $CommissionValue = CommissionValue::orderBy('id', 'ASC')->get();
        //dd($CommissionValue);
        $Commission = array();
        if ($Products->isNotEmpty()) {
            foreach($Products as $Product)
            {
                
                $Provider = Providers::where('id', $Product->provider_id)->get();
                
                foreach($CommissionValue as $Commission)
                {
                    $Commission = CommissionValue::where('product_id', $Product->id)->orderBy('id', 'ASC')->get();
                    $CommissionValue = $Commission;
                    //dd($CommissionValue);


                    //$Provider = Providers::where('id', $Product->provider_id)->get();
                    
                }
            }

            //dd($Provider);
            return view('content.show_products', compact('Products', 'Provider', 'CommissionValue'));
        }else{
            $Products = 0;
            $Provider = 0;
            $CommissionValue = 0;
            //dd($Provider);
            return view('content.show_products', compact('Products', 'Provider', 'CommissionValue'));
        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
