<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use App\Models\Network;
use App\Models\Providers;
use App\Models\ProvidersNetwork;
use Illuminate\Support\Facades\DB;
use App\Models\AssociateRequestWithSeller;
use App\Models\AssociateRequestWithSellerCollect;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->get();
        return view('content.users')->with('users', $users);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        $network = Network::all();
        $data = array();
        if ($network->isNotEmpty()) {
            foreach ($network as $red){
                $sender = User::where('id', $red->sender_user_id)->get();
                $invited = User::where('contract', 'FALSE')
                                ->where('id', $red->receiver_user_id)->get();

                if ($invited->isEmpty() === true){
                    $data = 0;
                }else{
                    $datas[] = [
                    'sender'=> $sender, 
                    'invited'=>  $invited,    
                    ];
                    //dd($datas);
                    $data = $datas;
                                      
                }
            }
            return view('content.pending_user', compact('data'));
        } else {
            $data = 0;
            return view('content.pending_user', compact('data'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        //dd($request->input());
        $contract = $request->input('contract');
        $id = $request->input('user');
        if ($contract !=null){
            User::find($id)->update(['contract' => 'CANCEL']);
        }else{
            User::find($id)->update(['contract' => 'TRUE']);
        }
        return redirect()->route('pending-user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function linkSellerPlacement()
    {
        $Providers = Providers::all();

        return view('content.link_seller', compact('Providers'));
    }

    public function sellerPlacementList(Request $request)
    {
        try {    
            $provider_id = $request->input('provider_id');
            $Providers = ProvidersNetwork::where('providers_id', $provider_id)->get();
            $response = ['data' => $Providers];
        } catch (\Exception $exception) {
            return response()->json([ 'message' => 'No hay Usuarios asociados a este Proveedor' ], 500);
        }
        return response()->json($response);

    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sellerPlacementStore(Request $request)
    {
        
        $date = Carbon::now();
        $date = $date->format('d/m/Y');
        //dd($date);
            $AssociateRequestWithSeller = AssociateRequestWithSeller::create([   
                'provider_id' => $request->provider,
                'seller_id' => $request->seller,
                'application_number' => $request->application_number,
                'signature_date' => $request->signature_date,
                'application_registration_date' => $date,
                "client_name"  => $request->client_name,
                "client_surnames"  => $request->client_surnames,

            
            ]);
            $this->sellerCollectStore($request);
            return redirect()->back()->with('success','El Vendedor se Vinculo Correctamente');

    }

    public function linkSellerCollect()
    {
        $Providers = Providers::all();

        return view('content.link_seller_collect', compact('Providers'));
    }

    public function sellerCollectList(Request $request)
    {
        try {    
            $provider_id = $request->input('provider_id');
            $Providers = ProvidersNetwork::where('providers_id', $provider_id)->get();
            $response = ['data' => $Providers];
        } catch (\Exception $exception) {
            return response()->json([ 'message' => 'No hay Usuarios asociados a este Proveedor' ], 500);
        }
        return response()->json($response);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sellerCollectStore(Request $request)
    {
        
        //dd($request->all());
        $date = Carbon::now();
        $date = $date->format('d/m/Y');
            $AssociateRequestWithSeller = AssociateRequestWithSellerCollect::create([   
                'provider_id' => $request->provider,
                'seller_id' => $request->seller,
                'application_number' => $request->application_number,
                'signature_date' => $request->signature_date,
                'application_registration_date' => $date,
                "client_name"  => $request->client_name,
                "client_surnames"  => $request->client_surnames,            
            ]);
            return redirect()->back()->with('success','El Vendedor se Vinculo Correctamente ');

    }


    public function profile(User $id)
    {
        $id = $id->id;
        $data = DB::table('networks')->where('receiver_user_id', $id)->get()->first();
        if ($data === null){
                $user = [
                    'sender_user_id'=> 0, 
                    'receiver_user_id'=>  0
                ];
                return view('content.profile')->with('user', $user);
            }else{
                $data1 = DB::table('users')->where('id', $data->sender_user_id)->get()->first();
                $user = [
                    'sender_receiver_user_id'=> $data->sender_user_id, 
                    'receiver_user_id'=>  $data->receiver_user_id,
                    'leader_name'=>  $data1->name,
                    'email'=>  $data1->email

                ];
                return view('content.profile')->with('user', $user);
        
            }
            
    }
}
