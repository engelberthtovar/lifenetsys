<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Network;
use App\Models\Providers;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Rules\Password;
//use Laravel\Fortify\Contracts\CreatesNewUsers;
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;


class InvitationRegisterController extends Controller
{
    

    public function store($params1, $params2 = null)
    {
        if(!empty($params2))
        {
            $params = $params1.'/'.$params2;
        }else {
            $params = $params1;
        }
        return view('auth.invitation-register')->with('params', $params);
    }


        /**
     * Validate and create a newly registered user.
     *
     *
     * @return \App\Models\User
     */
    public function create(Request $input)
    {   
        //dd($input->request->all());
        Validator::make($input->request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'string', 'max:20'],
            'ruc' => ['required', 'string', 'max:20'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        DB::transaction(function () use($input){
            $date = Carbon::now();
            $sender_user_id = User::where('code', $input['code'])->first();
            //$Providers = Providers::where('code', $input['code'])->first();

            $User = User::create([
                'name' => $input['name'],
                'dni' => $input['dni'],
                'ruc' => $input['ruc'],
                'email' => $input['email'],
                'email_verified_at' => $date,
                'code' => uniqid(),
                'password' => Hash::make($input['password']),
            ]); 
            Network::create([   
                'sender_user_id' =>  $sender_user_id->id,
                'sender_fullname' => $sender_user_id->name,
                'receiver_user_id' =>  $User->id,
                'receiver_fullname' => $User->name,
            ]);
            $User->assignRole('view_users');

        });

            return redirect()->route('dashboard');


        

       // return $user;
    }

        /**
     * Get the validation rules used to validate passwords.
     *
     * @return array
     */
    protected function passwordRules()
    {
        return ['required', 'string', new Password, 'confirmed'];
    }

}
