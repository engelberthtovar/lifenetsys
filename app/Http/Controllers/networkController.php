<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Network;
use App\Models\ProvidersContracts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class networkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
        $network = Network::all();
        $datax = array();
        if ($network->isNotEmpty()) {
            foreach ($network as $red){
                $sender = User::where('id', $red->sender_user_id)->get();
                $invited = User::where('id', $red->receiver_user_id)->get();
                $Contracts = ProvidersContracts::where('user_id', $red->receiver_user_id)->get();
                if ($Contracts->isNotEmpty()) {                    
                    $datas = [
                    'sender'=> $sender, 
                    'invited'=>  $invited, 
                    'contracts'=>  $Contracts,   
                    ];
                    $data[] = $datas;
                }else{
                    $datas = [
                        'sender'=> $sender, 
                        'invited'=>  $invited,
                        'contracts'=>  0,     
                        ];
                        $data[] = $datas;
                }
            } 
            return view('content.network', compact('data'));
        } else {
            $data = 0;
            return view('content.network', compact('data'));
        }
        
        
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
