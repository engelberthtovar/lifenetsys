<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Network;
use App\Models\Providers;
use App\Models\Postulations;
use Illuminate\Http\Request;
use App\Models\ProvidersNetwork;
use App\Models\ProvidersContracts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $User = Auth::user()->id;
        $Providers = Providers::all();
        //dd($Providers);
        $Postulations = Postulations::where('user_id', $User)->get();
                                    //->where('status', 'TRUE')->get();
        //dd($Postulations);

        if ($Postulations->isNotEmpty()) {
            return view('content.providers', compact('Providers', 'Postulations'));
        }else{
            $Postulations = 0;
            return view('content.providers', compact('Providers', 'Postulations'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $input)
    {
        //dd($input->request->all());
        Validator::make($input->request->all(), [
            'social_reason' => ['required', 'string', 'max:255'],
            'comercial_name' => ['required', 'string', 'max:255'],
            'ruc' => ['required', 'string', 'max:20'],
            'direction' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            "level_size"  => ['required', 'integer'],
            "size_depth"  => ['required', 'integer'],
            'administrative_contact_name' => ['required', 'string', 'max:255'],
            'administrative_contact_phone' => ['required', 'string', 'max:255'],
        ])->validate();
        Providers::create([   
            'social_reason' => $input['social_reason'],
            'comercial_name' => $input['comercial_name'],
            'ruc' => $input['ruc'],
            'direction' => $input['direction'],
            'email' => $input['email'],
            "level_size"  => $input['level_size'],
            "size_depth"  => $input['size_depth'],
            'administrative_contact_name' => $input['administrative_contact_name'],
            'administrative_contact_phone' => $input['administrative_contact_phone'],
        ]);
        

        return redirect()->route('providers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $Postulations = Postulations::where('status', 'FALSE')->get();
        //$Network = Network::all();
        //dd($Postulations);
        $data = array();
        if ($Postulations->isNotEmpty()) {
            foreach ($Postulations as $Post){
                $recipient_user = User::where('id', $Post->user_id)->get();
                //dd($recipient_user);
                $Network = Network::where('receiver_user_id', $recipient_user['0']->id)->get();
                $Network = $Network['0']->sender_user_id;
                //dd($Network);
                $head_user = User::where('id', $Network)->get();
                $Providers = Providers::where('id',$Post->providers_id)->get();
                 // --Esto se ativara cuando decidan cambiar la postulacion --//
                $ProvidersContracts = ProvidersContracts::where('user_id', $recipient_user['0']->id)->get();
                //$url = Storage::url($ProvidersContracts['0']->document);
                //dd($url);
                /*$head_user = User::where('contract', 'TRUE')
                                ->where('id', $Post->receiver_user_id)->get();*/
                if ($recipient_user->isEmpty() === true){
                    $data = 0;
                    //dd($data);
                }else{
                    //$Route = public_path() . '/storage/upload';
                    $Route = public_path('storage\upload');
                    //dd($Route);
                    $datas[] = [
                    'post'=> $Post,
                    'provider'=> $Providers,
                    'head_user'=> $head_user,  
                    'recipient_user'=>  $recipient_user,
                    // --Esto se ativara cuando decidan cambiar la postulacion --//
                    'route' => $Route,
                    'providers_contracts' => $ProvidersContracts    
                    ];
                    $data = $datas;
                    //dd($data);
                   // echo '<pre>' . var_export($data, true) . '</pre>';;
                                      
                }
            }
            //dd($data);
            //die;
            return view('content.providers_pending_users', compact('data'));
        } else {
            $data = 0;
            //dd($data);
            return view('content.providers_pending_users', compact('data'));
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function networkshow()
    {
        $ProvidersNetwork = ProvidersNetwork::all();
        $data = $ProvidersNetwork;
        return view('content.providers_network', compact('data'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $Providers = Providers::find($request->id);
        $Providers->social_reason = $request->filled('social_reason') ? $request->social_reason : $Providers->social_reason;
        $Providers->comercial_name = $request->filled('comercial_name') ? $request->comercial_name : $Providers->comercial_name;
        $Providers->ruc = $request->filled('ruc') ? $request->ruc : $Providers->ruc;
        $Providers->direction = $request->filled('direction') ? $request->direction : $Providers->direction;
        $Providers->administrative_contact_name = $request->filled('administrative_contact_name') ? $request->administrative_contact_name : $Providers->administrative_contact_name;
        $Providers->administrative_contact_phone = $request->filled('administrative_contact_phone') ? $request->administrative_contact_phone : $Providers->administrative_contact_phone;
        $Providers->email = $request->filled('email') ? $request->email : $Providers->email;
        $Providers->status = $request->filled('status') ? $request->status : $Providers->status;
        $Providers->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Providers = Providers::find($request->id);
        //dd($Providers);
        $Providers->status = '0';
        $Providers->save();
        //$Providers->delete();
        return redirect()->back();
    }
}
