<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Network;
use App\Models\Providers;
use Illuminate\Http\Request;
use App\Models\Postulations;
use App\Models\ProvidersNetwork;
use App\Models\ProvidersContracts;
use Illuminate\Support\Facades\Storage;


class PostulationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        
        $File = $request->file('file');
        //dd($request->all());
        $Route = public_path() . '/storage/upload';
        $FileName = uniqid() . $File->getClientOriginalName();
        $File->move($Route, $FileName);
        //dd($Route);
        ProvidersContracts::create([
            'user_id' => $request->user_id,
            'document' => $FileName,
        ]);
        //$request->session()->flash('notificacion', 'El Archivo ' .$File->getClientOriginalName(). ' Se Ha Guardado Correctamente');
        return redirect()->back();
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $File = $request->file('file');
        //dd($request->all());
        $Route = public_path() . '/storage/upload';
        $FileName = uniqid() . $File->getClientOriginalName();
        $File->move($Route, $FileName);
        //dd($Route);
        ProvidersContracts::create([
            'user_id' => $request->user_id,
            'document' => $FileName,
        ]);
        */
        Postulations::create([
            'user_id' => $request->user_id,
            'providers_id' => $request->provider_id,
            'status' => 'FALSE',
        ]); 
           
        //$request->session()->flash('notificacion', 'El Archivo ' .$File->getClientOriginalName(). ' Se Ha Guardado Correctamente');
        return redirect()->back();
        
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
       $Document = $request->document;
       $Route = public_path()."/storage/upload/".$Document;
       return response()->download($Route);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $Network = Network::where('receiver_user_id', $request->user_id)->get();
        $Head_User = User::where('id', $Network['0']->sender_user_id)->get();
        $Postulations = Postulations::where('user_id', $request->user_id)->latest()->first();
        //dd($Postulations);
        $Providers = Providers::where('id',$Postulations->providers_id)->get();
        //dd($Head_User['0']->name);
        //dd($Postulations);
        $Postulations->status = 'TRUE';
        $Postulations->save();
        ProvidersNetwork::create([
            'head_user_id' => $Head_User['0']->id,
            'head_user_fullname' => $Head_User['0']->name,
            'recipient_user_id' => $request->user_id,
            'recipient_fullname' => $request->user_name,
            'providers_id'=> $Providers['0']->id,
            'providers_comercial_name' => $Providers['0']->comercial_name,
        ]);
        return redirect()->back();
    }

    /** Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function refuse(Request $request)
   {
    $Postulations = Postulations::where('user_id', $request->user_id)->latest()->first();
    //dd($Postulations);
    //dd($Postulations);
    $Postulations->status = 'CANCEL';
    $Postulations->save();
    return redirect()->back();
   }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
