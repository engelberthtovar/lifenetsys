<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRoles()
    {
        $Role = Role::all();
        return view('content.create_role', compact('Role'));
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPermissions()
    {
        $Role = Permission::all();
        return view('content.create_permission', compact('Role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRole(Request $request)
    {   
        dd($request->create_role);
        $Role = Role::create(['name' => $request->create_role]);
        return back()->with('flash', 'El Role se creo Correctamente');
        //$Role = Role::all();

        //dd($Role);

        //$Role = Role::find('3')->givePermissionTo(Permission::findById('5'));
        //$Role = Role::first();
        //dd($User);
        //$Role = Role::create(['name' => 'view']);
        //$Role = Role::findById('3')->givePermissionTo(Permission::findById('5'));
        //$Permission = Permission::findById('5');
        //$Role->givePermissionTo($Permission);
        //Role::create(['name' => 'view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setRoleToUser(Request $request)
    {
        $Role = Role::find('3');
        $RoleName = $Role->name;
        $User = User::find('4');
        $User->assignRole($RoleName);
        return back()->with('flash', 'El Role se Asigno Correctamente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPermission(Request $request)
    {
        dd($request->create_permission);
        Permission::create(['name' => $request->create_permission]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setPermissionToUser(Request $request)
    {
        $Permission = Permission::find($request->permission_id);
        $PermissionName = $Permission->name;
        $User = User::find('4');
        $User->assignRole($PermissionName);
        return back()->with('flash', 'El Role se Asigno Correctamente');
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setPermissionToRole(Request $request)
    {
        
        
        Role::find($request->role_id)->givePermissionTo(Permission::find($request->permission_id));
        return back()->with('flash', 'El Permiso se Asigno Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
