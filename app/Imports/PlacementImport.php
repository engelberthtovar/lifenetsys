<?php

namespace App\Imports;

use App\Models\Placement;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PlacementImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

/**
 * Transform a date value into a Carbon object.
 *
 * @return \Carbon\Carbon|null
 */
public function transformDate($value, $format = 'd-m-Y')
{
    try {
        return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
    } catch (\ErrorException $e) {
        return \Carbon\Carbon::createFromFormat($format, $value);
    }
}

    public function model(array $row)
    {
        //dd($row);
        //$date = Carbon::now();
        return new Placement([
            'modalidad'                => str_replace("'", "", $row['modalidad']),
            'canal'                    => str_replace("'", "", $row['canal']),
            'grupo_familiar'           => str_replace("'", "", $row['grupo_familiar']),
            'codigo_unico_afiliado'    => str_replace("'", "", $row['codigo_unico_afiliado']),
            'codigo_persona'           => str_replace("'", "", $row['codigo_persona']),
            'apellidos_nombres'        => str_replace("'", "", $row['apellidos_y_nombres']),
            'fecha_nacimiento'         => $this->transformDate($row['fecha_nacimiento']),
            'sexo'                     => str_replace("'", "", $row['sexo']),
            'tipo_documento_identidad' => str_replace("'", "", $row['tipo_documento_identidad']),
            'numero_documento'         => str_replace("'", "", $row['numero_documento']),
            'codigo_ubigeo'            => str_replace("'", "", $row['codigo_ubigeo']),
            'distrito'                 => str_replace("'", "", $row['distrito']),
            'provincia'                => str_replace("'", "", $row['provincia']),
            'departamento'             => str_replace("'", "", $row['departamento']),
            'fecha_solicitud'          => $this->transformDate($row['fecha_solicitud']),
            'fecha_primer_pago'        => $this->transformDate($row['fecha_primer_pago']),
            'fecha_liq_recp'           => $this->transformDate($row['fecha_liqrecp']),
            'fecha_afiliacion'         => $this->transformDate($row['fecha_afiliacion']),
            'fecha_inicio_vigencia'    => $this->transformDate($row['fecha_inicio_vigencia']),
            'fecha_fin_vigencia'       => $this->transformDate($row['fecha_fin_vigencia']),
            'fecha_continuidad'        => $this->transformDate($row['fecha_continuidad']),
            'fecha_baja'               => $this->transformDate($row['fecha_baja']),
            'programa'                 => str_replace("'", "", $row['programa']),
            'rango_etario'             => str_replace("'", "", $row['rango_etario']),
            'tarifa'                   => str_replace("'", "", $row['tarifa']),
            'descripcion_tarifa'       => str_replace("'", "", $row['descripcion_tarifa']),
            'fumador'                  => str_replace("'", "", $row['fumador']),
            'frecuencia'               => str_replace("'", "", $row['frecuencia']),
            'cuota'                    => str_replace("'", "", $row['cuota']),
            'monto_soles'              => str_replace("'", "", $row['monto_soles']),
            'monto_dolares'            => str_replace("'", "", $row['monto_dolares']),
            'tipo_cambio'              => str_replace("'", "", $row['tipo_cambio']),
            'asesora_sede'             => str_replace("'", "", $row['asesora_sede']),
            'descripcion_asesora'      => str_replace("'", "", $row['descripcion_asesora']),
            'tipo_asesora'             => str_replace("'", "", $row['tipo_asesora']),
            'nombre_coordinador'       => str_replace("'", "", $row['nombre_cordinador']),
            'nombre_supervisor'        => str_replace("'", "", $row['nombre_supervisor']),
            'grupo_canal'              => str_replace("'", "", $row['grupo_canal']),
            'numero_afiliados'         => str_replace("'", "", $row['numero_afiliados']),
            'observacion'              => str_replace("'", "", $row['observacion']),
            'tipo_tarjeta'             => str_replace("'", "", $row['tipo_tarjeta']),
            'fecha_registro'           => $this->transformDate($row['fecha_registro']),
            'nro_solicitud'            => str_replace("'", "", $row['nro_solicitud']),
            'origen_venta'             => str_replace("'", "", $row['origen_venta']),
            //'provider_id'            => $row[provider_id],
            //'procesado'                => '0',
            //'fecha_importacion'       => $this->transformDate($date),
            
        ]);
    }
}
