<?php

namespace App\Imports;

use App\Models\Collect;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CollectImport implements ToModel, WithHeadingRow
{
    use Importable;

/**
 * Transform a date value into a Carbon object.
 *
 * @return \Carbon\Carbon|null
 */
public function transformDate($value, $format = 'd-m-Y')
{
    try {
        return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
    } catch (\ErrorException $e) {
        return \Carbon\Carbon::createFromFormat($format, $value);
    }
}
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dd($date);
        return new Collect ([
            'MesRecaudo'              => str_replace("'", "", $row['mesrecaudo']),
            'Asesor'                  => str_replace("'", "", $row['asesor']),
            'CodGrupoFam'             => str_replace("'", "", $row['codgrupofam']),
            'NroSolicitud'            => str_replace("'", "", $row['nrosolicitud']),
            'Modo'                    => str_replace("'", "", $row['modo']),
            'Frecuencia'              => str_replace("'", "", $row['frecuencia']),
            'NumCuotaProgramada'      => str_replace("'", "", $row['numcuotaprogramada']),
            'Programa'                => str_replace("'", "", $row['programa']),
            'FechaPrimerCobro'        => $this->transformDate($row['fechaprimercobro']),
            'MontoPagoMes'            => str_replace("'", "", $row['monto_pago_mes']),
            'CuotasMesPagadas'        => $this->transformDate($row['cuotasmespagadas']),
            //'fecha_importacion'       => $date,
            //'provider_id'             => $row['provider_id'],
        ]);
    }
}
