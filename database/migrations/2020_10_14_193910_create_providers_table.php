<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->id();
            $table->string('social_reason');
            $table->string('comercial_name');
            $table->string('ruc');
            $table->string('direction');
            $table->string('email')->unique();
            $table->string('administrative_contact_name');
            $table->string('administrative_contact_phone');
            $table->unsignedBigInteger('level_size');
            $table->unsignedBigInteger('size_depth');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
