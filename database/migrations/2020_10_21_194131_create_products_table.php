<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('provider_id');
            $table->string('product_name');      
            $table->string('description');
            $table->string('product_code');
            $table->string('referencial_value');
            $table->string('colocacion');
            $table->string('recaudo');
            $table->string('calculo_colocacion')->default('FIJO')->nullable();
            $table->string('calculo_recaudo')->default('FIJO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
