<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacementSellerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placement_seller_logs', function (Blueprint $table) {
            $table->id();
            $table->string('numero_solicitud');
            $table->string('grupo_familiar');
            $table->string('codigo_persona');
            $table->string('monto_soles');
            $table->string('fecha_solicitud');
            $table->string('seller_id');
            $table->string('provider_id');
            $table->string('product_code');
            $table->string('procesado')->default(0);          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placement_seller_logs');
    }
}
