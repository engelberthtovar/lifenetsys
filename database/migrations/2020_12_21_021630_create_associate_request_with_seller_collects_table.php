<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociateRequestWithSellerCollectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associate_request_with_seller_collects', function (Blueprint $table) {
            $table->id();
            $table->string('provider_id')->nullable(); 
            $table->string('seller_id')->nullable();
            $table->string('application_number')->nullable(); 
            $table->string('signature_date')->nullable(); 
            $table->string('application_registration_date')->nullable();
            $table->string('application_approval_date')->nullable();
            $table->string('client_name')->nullable();
            $table->string('client_surnames')->nullable();   
            $table->string('procesado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associate_request_with_seller_collects');
    }
}
