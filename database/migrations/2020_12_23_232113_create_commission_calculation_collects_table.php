<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionCalculationCollectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_calculation_collects', function (Blueprint $table) {
            $table->id();
            $table->string('commissionist_id')->nullable();
            $table->string('seller_id')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('grupo_familiar')->nullable();
            $table->string('numero_solicitud')->nullable();
            $table->string('monto')->nullable();
            $table->string('fecha_solicitud')->nullable();
            $table->string('fecha_importacion')->nullable();
            $table->string('monto_fijo')->nullable();
            $table->string('monto_porcentual')->nullable();
            $table->string('calculo')->nullable();          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_calculation_collects');
    }
}
