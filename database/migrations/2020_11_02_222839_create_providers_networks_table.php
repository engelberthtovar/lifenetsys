<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvidersNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers_networks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('head_user_id');
            $table->foreign('head_user_id')->references('sender_user_id')->on('networks');
            $table->string('head_user_fullname');
            $table->unsignedBigInteger('recipient_user_id');
            $table->foreign('recipient_user_id')->references('receiver_user_id')->on('networks');
            $table->string('recipient_fullname');
            $table->unsignedBigInteger('providers_id');
            $table->foreign('providers_id')->references('id')->on('providers');
            $table->string('providers_comercial_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers_networks');
    }
}
