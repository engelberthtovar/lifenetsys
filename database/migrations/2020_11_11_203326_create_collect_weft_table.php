<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectWeftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collect_weft', function (Blueprint $table) {
            $table->id();
            $table->string('MesRecaudo');
            $table->string('Asesor');
            $table->string('CodGrupoFam');
            $table->string('NroSolicitud');
            $table->string('Modo');
            $table->string('Frecuencia');
            $table->string('NumCuotaProgramada');
            $table->string('Programa');
            $table->string('FechaPrimerCobro');
            $table->string('MontoPagoMes');
            $table->string('CuotasMesPagadas');
            $table->string('migrado')->default(0);
            $table->unsignedBigInteger('provider_id')->nullable();
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->string('fecha_importacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect_weft');
    }
}
