<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColletSellerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collet_seller_logs', function (Blueprint $table) {
            $table->id();
            $table->string('Asesor');
            $table->string('CodGrupoFam');
            $table->string('NroSolicitud');
            $table->string('FechaPrimerCobro');
            $table->string('MontoPagoMes');
            $table->string('seller_id');
            $table->unsignedBigInteger('provider_id')->nullable();
            $table->string('procesado')->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collet_seller_logs');
    }
}
