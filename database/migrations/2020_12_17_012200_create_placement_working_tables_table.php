<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacementWorkingTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placement_working_tables', function (Blueprint $table) {
            $table->id();
            $table->string('modalidad')->nullable();
            $table->string('canal')->nullable();
            $table->string('grupo_familiar')->nullable();
            $table->string('codigo_unico_afiliado')->nullable();
            $table->string('codigo_persona')->nullable();
            $table->string('apellidos_nombres')->nullable();
            $table->string('fecha_nacimiento')->nullable();
            $table->string('sexo')->nullable();
            $table->string('tipo_documento_identidad')->nullable();
            $table->string('numero_documento')->nullable();
            $table->string('codigo_ubigeo')->nullable();
            $table->string('distrito')->nullable();
            $table->string('provincia')->nullable();
            $table->string('departamento')->nullable();
            $table->string('fecha_solicitud')->nullable();
            $table->string('fecha_primer_pago')->nullable();
            $table->string('fecha_liq_recp')->nullable();
            $table->string('fecha_afiliacion')->nullable();
            $table->string('fecha_inicio_vigencia')->nullable();
            $table->string('fecha_fin_vigencia')->nullable();
            $table->string('fecha_continuidad')->nullable();
            $table->string('fecha_baja')->nullable();
            $table->string('programa')->nullable();
            $table->string('rango_etario')->nullable();
            $table->string('tarifa')->nullable();
            $table->string('descripcion_tarifa')->nullable();
            $table->string('fumador')->nullable();
            $table->string('frecuencia')->nullable();
            $table->string('cuota')->nullable();
            $table->string('monto_soles')->nullable();
            $table->string('monto_dolares')->nullable();
            $table->string('tipo_cambio')->nullable();
            $table->string('asesora_sede')->nullable();
            $table->string('descripcion_asesora')->nullable();
            $table->string('tipo_asesora')->nullable();
            $table->string('nombre_coordinador')->nullable();
            $table->string('nombre_supervisor')->nullable();
            $table->string('grupo_canal')->nullable();
            $table->string('numero_afiliados')->nullable();
            $table->string('observacion')->nullable();
            $table->string('tipo_tarjeta')->nullable();
            $table->string('fecha_registro')->nullable();
            $table->string('nro_solicitud')->nullable();
            $table->string('origen_venta')->nullable();
            $table->unsignedBigInteger('provider_id')->nullable();
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->string('procesado')->default(0);
            $table->string('fecha_importacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placement_working_tables');
    }
}
