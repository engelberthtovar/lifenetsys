<?php

namespace Database\Seeders;

use App\Models\User;
use DateTime;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions_array = [];
        array_push($permissions_array, Permission::create(['name' => 'Create_Users']));
        array_push($permissions_array, Permission::create(['name' => 'Edit_Users']));
        array_push($permissions_array, Permission::create(['name' => 'Delete_Users']));
        
        //
        
        //
        $ViewUsersPermissions = Permission::create(['name' => 'View_Users']);
        $StandardPermissions = Permission::create(['name' => 'View']);
        //
        array_push($permissions_array, $ViewUsersPermissions);
        //
        $superAdminRole = Role::create(['name' => 'super_admin']);
        $ViewUsersRole = Role::create(['name' => 'view_users']);
        //$StandardUsersRole = Role::create(['name' => 'view']);

        //
        $superAdminRole->syncPermissions($permissions_array);
        $ViewUsersRole->givePermissionTo($ViewUsersPermissions);
        //$StandardUsersRole->givePermissionTo($StandardPermissions);


        $UserSuperAdmin = User::create([
            'name' => 'Admin',
            'dni' => '123456789',
            'ruc' => '12345678910',
            'email' => 'Admin@admin.com',
            'email_verified_at' => date('now'),
            'code' => uniqid(),
            'password' => Hash::make('admin'),
        ]);
        $UserSuperAdmin->assignRole('super_admin');
        $UserDevop = User::create([
            'name' => 'Developer',
            'dni' => '999999999',
            'ruc' => '99999999999',
            'email' => 'developer@admin.com',
            'email_verified_at' => date('now'),
            'code' => uniqid(),
            'password' => Hash::make('developer'),
        ]);
        $UserDevop->assignRole('super_admin');
    }
}


