<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AccessController;
use App\Http\Controllers\PlotsController;
use App\Http\Controllers\networkController;
use App\Http\Controllers\contractsController;
use App\Http\Controllers\ProvidersController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\PostulationsController;
use App\Http\Controllers\InvitationMailController;
use App\Http\Controllers\InvitationRegisterController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;

//use Laravel\Fortify\Features;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('storage-link', function(){
        
if(file_exists(public_path('storage'))){
        return ' Ya existe';
    }

    app('files')->link(
            storage_path('app/public'), public_path('storage')
    );

        return ' no existia y lo cree';
    

    });


Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:web', 'verified'])->get('/dashboard', function () {
    return view('dashboard.dashboard');
})->name('dashboard');


        //return view('content.users');



//----Emails Intivations system Routes ----//
Route::get('/invitation', [InvitationMailController::class, 'index'])->name('invitation');
Route::post('/store/mail', [InvitationMailController::class, 'store'])->name('store.mail');
Route::get('/sussesfull/mail', [InvitationMailController::class, 'sendInvitation']);
Route::get('/invitation/store/{params1}/{params2?}', [InvitationRegisterController::class, 'store'])->name('invitation.store');
Route::post('/invitation/register', [InvitationRegisterController::class, 'create'])->name('invitation.register');

//----Roles And Permissions Routes----//
Route::group(['middleware' => ['role:super_admin']], function () {
Route::get('/register', [RegisteredUserController::class, 'create'])
->name('register');
Route::post('/register', [RegisteredUserController::class, 'store']);
    });

//---- Users Admin----//
Route::group(['middleware' => ['role:super_admin']], function () {
Route::get('/users', [UserController::class, 'index'])
    ->name('admin-user');
Route::post('/users',  [UserController::class, 'update'])
->name('pending-user');
Route::get('/pending',  [UserController::class, 'show'])
    ->name('show-user');
Route::get('/network', [networkController::class, 'index'])
->name('network');

//---- Admin Providers ----//
Route::get('/add/providers', function () {
    return view('content.add_provider');
})->name('providers.form');

Route::post('/add/providers',  [ProvidersController::class, 'create'])
->name('add.providers');

Route::post('/update-providers',  [ProvidersController::class, 'update'])
->name('update.provider');
Route::post('/destroy-providers',  [ProvidersController::class, 'destroy'])
->name('destroy.provider');

//--- Pending Users Providers ---//
Route::get('/pending-users-providers',  [ProvidersController::class, 'show'])
->name('prouserspen');

//--- Providers Network ---//
Route::get('/providers-network',  [ProvidersController::class, 'networkshow'])
->name('pronetwork');

});

//---- Providers ----//
Route::get('/providers',  [ProvidersController::class, 'index'])
->name('providers');


//---- Products ----//
Route::get('/products',  [ProductsController::class, 'index'])
->name('add_products');

Route::post('/add-products',  [ProductsController::class, 'store'])
->name('store_products');

Route::get('/show-products',  [ProductsController::class, 'show'])
->name('show_products');




//---- Tramas ----//
Route::get('/placement-upload',  [PlotsController::class, 'indexPlacement'])
->name('upload_placement');

Route::post('/placement-upload',  [PlotsController::class, 'storePlacement'])
->name('import_placement');

Route::get('/collections-upload',  [PlotsController::class, 'indexCollections'])
->name('upload_collections');

Route::post('/collections-upload',  [PlotsController::class, 'storeCollections'])
->name('import_collections');


Route::get('/migrate_placement',  [PlotsController::class, 'MigratePlacement'])
->name('migrate_placement');

Route::get('/migrate_colletions',  [PlotsController::class, 'MigrateCollections'])
->name('migrate_colletions');

Route::get('/search_seller',  [PlotsController::class, 'showContracts'])
->name('search_seller');

Route::get('/seller_lists',  [PlotsController::class, 'sellerLists'])
->name('seller_list');

Route::get('/process_request_placement',  [PlotsController::class, 'showPlacementContracts'])
->name('process_request_placement');

Route::get('/process_request_collect',  [PlotsController::class, 'showCollectContracts'])
->name('process_request_collect');

Route::get('/calculate_collect',  [PlotsController::class, 'CommissionCalculationCollect'])
->name('calculate_collect');

Route::get('/calculate_placement',  [PlotsController::class, 'CommissionCalculationPlacement'])
->name('calculate_placement');

/*Route::get('/calculate_placement',  [PlotsController::class, 'funcioncalculo'])
->name('calculate_placement');
*/

Route::get('/commissions_placement_report',  [PlotsController::class, 'CommissionsPlacementReport'])
->name('commissions_placement_report');

Route::get('/consolidated_commissions_placement_report',  [PlotsController::class, 'ConsolidatedCommissionsPlacementReport'])
->name('consolidated_commissions_placement_report');

Route::get('/commissions_collect_report',  [PlotsController::class, 'CommissionsCollectReport'])
->name('commissions_collect_report');

Route::get('/consolidated_commissions_collect_report',  [PlotsController::class, 'ConsolidatedCommissionsCollectReport'])
->name('consolidated_commissions_collect_report');




//---- End Roles And Permissions Routes----//

//Route::middleware(['auth:sanctum', 'verified'])->get('/user',  [UserController::class, 'profile'])->name('userp');
//Route::middleware(['auth:sanctum', 'verified'])->get('/users',  [UserController::class, 'index'])->name('admin-user');
//Route::middleware(['auth:sanctum', 'verified'])->get('/pending',  [UserController::class, 'show'])->name('show-user');


//route::get('users', [UserController::class, 'index'])->name('show-user');
route::get('users/{id}', [UserController::class, 'show'])->where(['id' => '[0-9]+']);
route::get('users/crear',  [UserController::class, 'create']);
route::post('users/crear', [UserController::class, 'store']);

//---- Network ----//
//Route::middleware(['auth:web', 'verified'])->get('/network',  [networkController::class, 'index'])->name('network');

//---- Users----//
Route::middleware(['auth:web', 'verified'])->get('/welcome', function () {
    return view('dashboard.welcome');
})->name('welcome');
Route::middleware(['auth:web', 'verified'])->get('/profile/{id}',  [UserController::class, 'profile'])
->name('view-profile');
Route::middleware(['auth:web', 'verified'])->get('/user-profile', function () {
    return view('content.profile');
})->name('profile');


//---- Contracts ----//
Route::middleware(['auth:web', 'verified'])->get('/contracts',  [contractsController::class, 'index'])
->name('contratcs');


//---- Postulations ----//
Route::middleware(['auth:web', 'verified'])->post('/postulations',  [PostulationsController::class, 'store'])
->name('postulations');

Route::middleware(['auth:web', 'verified'])->post('/download-Contract',  [PostulationsController::class, 'download'])
->name('postul-download');

Route::middleware(['auth:web', 'verified'])->post('/upload-Contract',  [PostulationsController::class, 'upload'])
->name('postul-upload');

Route::middleware(['auth:web', 'verified'])->post('/update-postulations',  [PostulationsController::class, 'update'])
->name('postul-update');

Route::middleware(['auth:web', 'verified'])->post('/refuse-postulations',  [PostulationsController::class, 'refuse'])
->name('postul-refuse');

//---- Vendedores ----//

Route::get('/show_placement_link',  [UserController::class, 'linkSellerPlacement'])
->name('seller_placement_show');

Route::get('/show_placement_list',  [UserController::class, 'sellerPlacementList'])
->name('seller_placement_list');

Route::post('/seller_placement_store',  [UserController::class, 'sellerPlacementStore'])
->name('seller_placement_store');


Route::get('/show_collect_link',  [UserController::class, 'linkSellerCollect'])
->name('seller_collect_show');

Route::get('/show_collect_list',  [UserController::class, 'sellerCollectList'])
->name('seller_collect_list');

Route::post('/seller_collect_store',  [UserController::class, 'sellerCollectStore'])
->name('seller_collect_store');



//---- Products ----//
Route::middleware(['auth:web', 'verified'])->get('/contracts',  [contractsController::class, 'index'])
->name('contratcs');

//---- Permisos And Roles ----//
Route::middleware(['auth:web', 'verified'])->get('/view-Roles',  [AccessController::class, 'indexRoles'])
->name('View.Roles');
Route::middleware(['auth:web', 'verified'])->post('/Roles',  [AccessController::class, 'createRole'])
->name('Roles');
Route::middleware(['auth:web', 'verified'])->get('/view-Permissions',  [AccessController::class, 'indexPermissions'])
->name('View.Permissions');
Route::middleware(['auth:web', 'verified'])->post('/Permissions',  [AccessController::class, 'createPermission'])
->name('Permissions');
