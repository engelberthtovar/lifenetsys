@include('skeleton.header')
   <body>
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            @include('topbar.topbar')
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                    <!-- Start Content-->
                    
                    <div class="container-fluid">
                        

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">LifeConSys</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Páginas</a></li>
                                            <li class="breadcrumb-item active">Bienvenido</li>
                                        </ol>
                                        @include('alerts.success')
                                        @include('alerts.errors')
                                    </div>
                                    <h4 class="page-title">LifeConSys</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
                        <div class="row justify-content-center mt-4">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="error-ghost text-center">
                                    <img src="assets/images/lifeconnections.png" width="300" alt="error-image"/>                                    
                                </div>

                                <div class="text-center">
                                    <h3 class="mt-4 text-uppercase font-weight-bold">Bienvenido </h3>
                                    <!--<p class="text-muted mb-0 mt-3" style="line-height: 20px;">La aplicación se encuentra actualmente en Desarrollo</p>-->

                                    <!--<a class="btn btn-info mt-3" href="index.html"><i class="mdi mdi-reply mr-1"></i> Return Home</a>-->
                                </div>      
        
                            </div> <!-- end col -->
                        </div>
                    </div> <!-- container -->
                                    <!-- Footer Start -->
                @include('skeleton.footer')
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
    </body>
</html>