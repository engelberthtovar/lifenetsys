@include('skeleton.header')
<body class="authentication-bg authentication-bg-pattern">
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <div class="account-pages mt-5 mb-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="card">

                                    <div class="card-body p-4">
                                        
                                        <div class="text-center w-75 m-auto">
                                            <a href="index.html">
                                                <span><img src="{{ asset('assets/images/lifeconnections.png') }}" alt="" height="130"></span>
                                            </a>
                                            <p class="text-muted mb-4 mt-3">Conectando con todos.</p>
                                        </div>

                                        <h5 class="auth-title">Registro de  Usuario</h5>

                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="fullname">{{ __('Nombre y Apellido') }}</label>
                                                <input class="form-control" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" required/>
                                            </div>
                                            <div class="form-group">
                                                <label for="dni">{{ __('Dni') }}</label>
                                                <input class="form-control" type="text" name="dni" :value="old('dni')" required/>
                                            </div>
                                            <div class="form-group">
                                                <label for="ruc">{{ __('Ruc') }}</label>
                                                <input class="form-control" type="text" name="ruc" :value="old('ruc')" required/>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">{{ __('Email') }}</label>
                                                <input class="form-control" type="text"  name="email" :value="old('email')" required/>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">{{ __('Contraseña') }}</label>
                                                <input class="form-control" type="password" name="password" required autocomplete="new-password"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="confirm_password">{{ __('Confirmar Contraseña') }}</label>
                                                <input class="form-control" type="password" name="password_confirmation" required autocomplete="new-password"/>
                                            </div>
                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-danger btn-block" type="submit"> Registrar</button>
                                            </div>

                                        </form>

                                    </div> <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>


                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer footer-alt">
            <p class="text-center" style="margin-left:15%;">2020 &copy; Mimirwell <a href="" class="text-center">&nbsp;&hearts;</a> </p>
            </footer>

        <!-- Vendor js -->
        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>
</body>            

