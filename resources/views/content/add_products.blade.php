@include('skeleton.header_products')
<style type="text/css">
	#regiration_form fieldset:not(:first-of-type) {
		display: none;
	}
  </style>
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Proveedores</a></li>
                                    <li class="breadcrumb-item active">Agregar Producto</li>
                                </ol>
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <h4 class="page-title">Agregar Producto</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->
     
                
                <div class="account-pages mt-5 mb-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="card">

                                    <div class="card-body p-4">
                                        
                                        <div class="text-center w-75 m-auto">
                                            <a href="index.html">
                                                <span><img src="{{ asset('assets/images/lifeconnections.png') }}" alt="" height="130"></span>
                                            </a>
                                            <p class="text-muted mb-4 mt-3">Conectando con todos.</p>
                                        </div>

                                        <h5 class="auth-title">Registrar Producto</h5>
                                        
                                        <form id="regiration_form" method="POST" action="{{ route('store_products') }}">
                                            @csrf
                                            <fieldset id="provider">
                                                <div class="form-group">
                                                    <select class="wide" data-plugin="customselect" name="provider_id" id="provider_id">
                                                        <option name="0" value="0">Seleccione un Proveedor</option>
                                                       @foreach ($Providers as $Provider) 
                                                        <option value="{{ $Provider->id }}" data-level="{{ $Provider->size_depth }}">{{ $Provider->comercial_name }}</option> 
                                                       @endforeach 
                                                    </select>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="fullname">Nombre del producto (Programa)</label>
                                                    <input class="form-control" type="text" name="product_name" id="product_name" placeholder="Nombre del producto" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="fullname">Descripción </label>
                                                    <input class="form-control" type="text" name="description" id="description" placeholder="Descripción del Producto" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="fullname">Código del Producto </label>
                                                    <input class="form-control" type="text" name="code" id="code" placeholder="Código del Producto" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="fullname">Valor referencial</label>
                                                    <input class="form-control" type="text" name="referencial_value" id="referencial_value" placeholder="Valor referencial" required>
                                                </div>
                                                <div class="form-group">
                                                    <select class="wide" name="select" id="inputSelect" data-plugin="customselect">
                                                        <option name="0" value="0">Seleccione un Tipo de Comisión</option>
                                                        <option value="1">Comisión de Colocaión</option>
					                                    <option value="2">Comisión de Recaudo</option>
					                                    <option value="3">Comisión de Colocaión y Comisión de Recaudo</option>
                                                    </select>
                                                    <div class="clearfix"></div>
                                                </div>
                                                    <input type="button" name="next" class="next btn btn-info" value="Siguiente" />
                                            </fieldset>
                                            
                                            <fieldset id="colocacion">
                                                <h2> Comisión de Colocaión:</h2>
                                            <!--
                                                    <div class="form-group">
                                                    <label for="fName">Niveles de Colocaión</label>
                                                    <input type="text" class="form-control" name="nivel_colocacion" id="nivel_colocacion" placeholder="Niveles de Colocaión" >
                                                </div>
                                            -->
                                            
                                                <div class="form-group" id="monto_colocacion">
                                                    <label for="lName" class="monto_colocacion">Monto Por Nivel</label>
                                                    <!--<input type="text" class="form-control" name="monto_colocacion" id="monto_colocacion" placeholder="Monto de Colocaión" >-->
                                                </div>

                                                <div class="form-group">
                                                    <div class="radio radio-success mb-2 ">
                                                        <input name="calculo_colocacion" value="%" id="radio1" type="radio" >
                                                            <label for="radio1">
                                                            <i class="fas fa-percent"> Seleccione Si el Monto es Porcentual</i>
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-success mb-2 ">
                                                        <input name="calculo_colocacion" value="FIJO" id="radio2" type="radio" >
                                                            <label for="radio2">
                                                            <i class="fas fa-equals"> Seleccione Si el Monto es Fijo</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <input type="button" name="previous" class="previous btn btn-warning" value="Previo" />
                                                <input type="button" name="next" class="next btn btn-info" value="Siguiente" hidden="true" id="next1"/>
                                                <button class="btn btn-success float-sm-right " type="submit" hidden="true" id="submit_colocacion"> Registrar</button>
                                            </fieldset>
                                            
                                            <fieldset id="recaudo">
                                                <h2> Comisión de Recaudo:</h2>
                                            <!--
                                                <div class="form-group">
                                                    <label for="fName">Niveles de Recaudo</label>
                                                    <input type="text" class="form-control" name="nivel_recaudo" id="nivel_recaudo" placeholder="Niveles de Recaudo" >
                                                </div>
                                            -->
                                                <div class="form-group" id="monto_recaudo">                                                   
                                                    <label for="lName" class="monto_recaudo">Monto Por Nivel</label>
                                                    
                                                </div>        
                                                    <div class="form-group">
                                                        <div class="radio radio-success mb-2 ">
                                                            <input name="calculo_recaudo" value="%" id="radio3" type="radio" >
                                                                <label for="radio3">
                                                                <i class="fas fa-percent"> Seleccione Si el Monto es Porcentual</i>
                                                            </label>
                                                        </div>
                                                        <div class="radio radio-success mb-2 ">
                                                            <input name="calculo_recaudo" value="FIJO" id="radio4" type="radio" >
                                                                <label for="radio4">
                                                                <i class="fas fa-equals"> Seleccione Si el Monto es Fijo</i>
                                                            </label>
                                                        </div>
                                                    </div>                                              
                                                    <input type="button" name="previous" class="previous btn btn-warning float-sm-left" value="Previo" /> 
                                                    <input type="button" name="next" class="next btn btn-info" value="Siguiente" hidden="true" id="next2"/>
                                                    <button class="btn btn-success float-sm-right " type="submit" hidden="true" id="submit_recaudo"> Registrar</button>
                                            </fieldset>
                                        </form>

                                    </div> <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>

                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
    
    @include('skeleton.footer_products')
    
 