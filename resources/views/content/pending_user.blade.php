
@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">    
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                <li class="breadcrumb-item active">Usuarios Pendientes</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Usuarios Pendientes</h4>
                    </div> 
                </div><!-- end page title -->
            </div><!-- end row -->
           
            
           
        @if ($data !== 0 )  
        
               
            <!-- Users Section-->
            <div class="row">   
                <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Usuarios Pendientes</h4>
                                        <p class="text-muted font-13 mb-4">
                                        </p>
                                        
                                    
                                        <table id="datatable-buttons" class="table table-striped dt-resSponsive nowrap">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Miembro</th>
                                                <th colspan="2">Nivel Superior</th>
                                                <th>Estado</th>
                                                <th>RUC - Miembro</th>
                                                <th>Proveedor</th>
                                                <th>Aprobar Contrato</th>
                                                <th>Rechazar Contrato</th>
                                            </tr>
                                            </thead>
                                            @foreach ($data as $user)      
                                                @if ($user['invited'] !== 'null')
                                                
                                                    <tbody>                                                
                                                        <tr>
                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{$user['invited']['0']->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $user['invited']['0']->name }}</h5>
                                                                <p class="mb-0 text-muted"><small>Miembro</small></p>
                                                            </td>
                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{$user['sender']['0']->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $user['sender']['0']->name }}</h5>
                                                                <p class="mb-0 text-muted"><small>Nivel Superior</small></p>
                                                            </td>
                                                            <td>
                                                                @if($user['invited']['0']->contract == 'TRUE')
                                                                <span class="badge badge-light-success"> Firmado</span>
                                                                @elseif ($user['invited']['0']->contract == 'CANCEL')
                                                                <span class="badge badge-light-danger">Rechazado</span>
                                                                @else  
                                                                <span class="badge badge-light-warning">Pendiente</span> 
                                                                @endif
                                                            </td>

                                                            <td>
                                                            {{ $user['invited']['0']->ruc }}
                                                            </td>

                                                            <td>
                                                                LifeConnections
                                                            </td>

                                                            <td>
                                                                <!-- Center modal -->
                                                                <button id="confirmar" type="button" class="btn btn-info waves-effect waves-light" data-id="{{ $user['invited']['0']->id }}" data-toggle="modal" data-target=".bs-example-modal-center">Confirmar Contrato</button>

                                                            </td>
                                                            <td>
                                                                <!-- Center modal -->
                                                                <button id="denegar" type="button" class="btn btn-danger waves-effect waves-light"  data-id="{{ $user['invited']['0']->id }}" data-toggle="modal" data-target=".bs-modal-refuse">Rechazar Contrato</button>

                                                            </td>

                                                    </tr>                                                   
                                                </tbody>
                                                @endif
                                            @endforeach                                    
                                        </table>     
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                                 
                        <form method="POST" action="{{ route('pending-user') }}">
                                @csrf
                            <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myCenterModalLabel">Aprobar Contrato</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                       <!-- <div class="modal-body">
                                        <p class="text-muted font-13 mt-3 mb-2 text-center">Proveedores</p>
                                            <div class="text-center">
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input name="Oncosalud" type="checkbox" id="inlineCheckbox1" :value="Oncosalud">
                                                    <label for="inlineCheckbox1"> Oncosalud </label>
                                                </div>
                                                <div class="checkbox checkbox-purple form-check-inline">
                                                    <input name="Comercio" type="checkbox" id="inlineCheckbox2" :value="Comercio" >
                                                    <label for="inlineCheckbox2"> El Comercio </label>
                                                </div>
                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input name="Mapfre" type="checkbox" id="inlineCheckbox3" :value="Mapfre">
                                                    <label for="inlineCheckbox3"> Mapfre </label>
                                                </div>
                                            </div>   -->                                                                                
                                        <div class="modal-footer" id="div-confirmar">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                           
                                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </form>
                               
                        <form method="POST" action="{{ route('pending-user') }}">
                                @csrf
                            <div class="modal fade bs-modal-refuse" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myCenterModalLabel">Rehazar Contrato</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            Esta Seguro ?                                          
                                        </div>                                       
                                        <div class="modal-footer" id="div-denegar">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            <input type="hidden" name="contract" value="CANCELAR" />
                                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </form>

                        @if(count($data)>5)     
                            </div> <!-- end row-->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-right">
                                            <ul class="pagination pagination-rounded justify-content-end">
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                                        <span aria-hidden="true">«</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                                        <span aria-hidden="true">»</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        @endif
            </div>
            <!-- end row -->

            <!-- Modal -->
           <!-- <div id="custom-modal" class="modal-demo">
                <button type="button" class="close" onclick="Custombox.modal.close();">
                    <span>&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="custom-modal-title">Add Members</h4>
                <div class="custom-modal-text text-left">
                    <form>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input type="text" class="form-control" id="location" placeholder="Enter location">
                        </div>
                        <div class="form-group">
                            <label for="skill">Skill</label>
                            <input type="text" class="form-control" id="skill" placeholder="Enter skill with comma">
                        </div>
                        <div class="form-group">
                            <label for="about-details">About </label>
                            <textarea class="form-control" id="about-details" rows="3" placeholder="Enter about"></textarea>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light m-l-10"
                                onclick="Custombox.modal.close();">Cancel</button>
                        </div>
                    </form>
                </div>
            </div> -->

        @else
            <h4 class="modal-title text-center">No hay Usuarios Pendientes</h4>
        @endif 
        </div> <!-- container -->

    </div> <!-- content -->
            </div><!-- end wrapper -->
    </div><!-- end vue id -->
          
@include('skeleton.footer')

<script>
$('button[id=confirmar]').on('click',function () {
    id = $(this).data("id");
    console.log(id);
  //alert($(this).data("id"));
  innerhtml = '';
  innerhtml += "<input type='hidden' name='user' value='"+id+"'/>" ;
  $('#div-confirmar').append(innerhtml);
});

$('button[id=denegar]').on('click',function () {
    id = $(this).data("id");
    console.log(id);
  //alert($(this).data("id"));
  innerhtml = '';
  innerhtml += "<input type='hidden' name='user' value='"+id+"'/>" ;
  $('#div-denegar').append(innerhtml);
});

</script>