@include('skeleton.header')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                    <li class="breadcrumb-item active">Listado de Proveedores</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Listado de Proveedores</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->
            @role('view_users')  
            @foreach($Providers as $data)
                <div class="row ">
                    <div class="col-lg-5" style="margin-left:30%">
                        <div class="text-center card-box">
                            <div class="pt-2 pb-2">
                                    <img src="https://ui-avatars.com/api/?name={{ $data->comercial_name }}&amp;color=7F9CF5&amp;background=EBF4FF" class="rounded-circle img-thumbnail avatar-xl"
                                    alt="profile-image">
                                    <br>
                                    <br>
                                <div>
                                    <p style="margin-bottom:2px;" class="text-muted"> Nombre Comercial :</p>
                                    <h4 class="mt-1 text-dark">{{ $data->comercial_name }}</h4>
                                </div>
                                <br>
                                <div>
                                    <p style="margin-bottom:2px;" class="text-muted"> Razón Social :</p>
                                    <h4 class="mt-1 text-dark">{{ $data->social_reason }}</h4>
                                </div>
                                <br>
                                <div>
                                    <p class="text-muted"><i class="mdi mdi-map-marker-outline"></i> Dirección :</p>
                                    <p style="margin-top:1px;" class="text-muted">{{ $data->direction }}.</p>
                                </div>
                                <div class="form-group">
                                    <div class="row mt-3">
                                        <div class="col-4">
                                            <div class="mt-3">
                                                <h5>Contacto</h5>
                                                <p class="mb-0 text-muted text-truncate">{{ $data->administrative_contact_name }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mt-3">
                                                <h5>email</h5>
                                                <p class="mb-0 text-muted text-truncate">{{ $data->email }}</p>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mt-3">
                                                <h5>Teléfono</h5>
                                                <p class="mb-0 text-muted text-truncate">{{ $data->administrative_contact_phone }}</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div>
                                <!-- Modal Postulacion -->
                    <!--        <form method="POST" action="{{ route('postulations') }}" enctype="multipart/form-data">    
                                @csrf
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel">Importar Contrato</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h6>Subir Contrato</h6>
                                                        <p>Haz Click en el Boton "Seleccionar Archivo", busca con el nombre"Contrtato.pdf" seleccionalo y luego click en Confirmar Postulación .</p>
                                                        <p>(El Archivo debe llamarse "Contrato" y ser un Archivo "PDF")</p>
                                                        <hr>
                                                            <div class="form-group">                                                                                                                   
                                                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>
                                                                <input type="hidden" name="provider_id" value="{{ $data->id }}"/>
                                                                <div class="row">
                                                                    <div class="col-sm-4 ">
                                                                    <div class="invoiceBox">
                                                                    <label for="file">
                                                                        <div class="boxFile" data-text="Seleccionar Archivo">
                                                                        Seleccionar Archivo
                                                                        </div>
                                                                    </label>
                                                                    <!--<input id="file" multiple="" name="file" type="file" size="600"  accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff">-->
                                                                   <!-- <input id="file" name="file" type="file" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" class="btn btn-purple btn-large"/>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                                <!--<input id="file" name="file" type="file" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" class="btn btn-purple btn-large"/>
                                                                <!--<button name="file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="btn btn-purple btn-large">Subir Contrato</button>-->
                                                            <!--</div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <!--<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>-->
                                                            <!--<button type="submit" class="btn btn-success btn-block">Confirmar Postulación</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                               <!-- </div><!-- /.modal-dialog -->
                                            <!--</div><!-- /.modal -->  
                         <!--   </form> -->

                            @if(Session::has('notificacion'))
                                <h5>{{Session::get('notificacion')}}</h5>
                            @endif
                                @if($Postulations === 0)
                                        <form method="POST" action="{{ route('postulations') }}">    
                                                @csrf
                                                <div class="form-group">                                                    
                                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>
                                                    <!--<input type="hidden" name="user_name" value="{{ Auth::user()->name }}"/>-->
                                                    <input type="hidden" name="provider_id" value="{{ $data->id }}"/>
                                                    <!--<input type="hidden" name="comercial_name" value="{{ $data->comercial_name }}"/>-->
                                                    <button type="submit" class="btn btn-success btn-block">Postular</button>
                                                    <!--<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal" >Postular</button>-->
                                                </div>
                                        </form>
                                    @else 
                                    @foreach($Postulations as $Postulation)                               
                                    <!--@dump($data->id, $Postulation->providers_id)-->
                                        <!--@dump($Postulation)-->
                                        @if($data->id !== $Postulation->providers_id && $Providers->count() !== $Postulations->count())
                                        <form method="POST" action="{{ route('postulations') }}">    
                                            @csrf
                                            <div class="form-group">                                                    
                                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>
                                                <!--<input type="hidden" name="user_name" value="{{ Auth::user()->name }}"/>-->
                                                <input type="hidden" name="provider_id" value="{{ $data->id }}"/>
                                                <!--<input type="hidden" name="comercial_name" value="{{ $data->comercial_name }}"/>-->
                                                <!--<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal">Postular</button>-->
                                                <button type="submit" class="btn btn-success btn-block">Postular</button>
                                            </div>
                                        </form>
                                        @elseif ($Postulation->status === 'CANCEL' && $data->id === $Postulation->providers_id)
                                            <div class="form-group">
                                                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal" disabled>Rechazado</button>
                                            </div>
                                        @elseif ($Postulation->status === 'TRUE' && $data->id === $Postulation->providers_id)
                                        <div class="form-group">
                                                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal" disabled>Miembro</button>
                                        </div>
                                        @elseif ($Postulation->status === 'FALSE' && $data->id === $Postulation->providers_id)
                                        <div class="form-group">
                                            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#myModal" disabled>Pendiente</button>
                                        </div>
                                        @endif
                                    @endforeach 
                                @endif                                                              
                            </div> <!-- end .padding -->
                        </div> <!-- end card-box-->
                    </div> <!-- end col -->
                </div> <!-- end row-->               
            @endforeach
            @endrole
            @role('super_admin')
            <div class="row">   
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Listado de Proveedores</h4>
                                        <p class="text-muted font-13 mb-4">
                                        </p>
                                                                           
                                        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Nombre Comercial</th>
                                                <th colspan="2">Razón Social</th>
                                                <th class="text-center">Contacto</th>
                                                <th class="text-center">email</th>
                                                <th class="text-center">Teléfono</th>
                                                <th class="text-center">Dirección</th>
                                                <th class="text-center">Editar</th>
                                                <th class="text-center">Anular</th>
                                            </tr>
                                            </thead>
                                            @foreach ($Providers as $data)
                                                        
                                                @if ($data !== 'null')                                          
                                                    <tbody>                                                
                                                        <tr>
                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{ $data->comercial_name }}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $data->comercial_name }}</h5>
                                                                <p class="mb-0 text-muted"><small>Seguros . . .</small></p>
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $data->social_reason }}</h5>
                                                                <!--<p class="mb-0 text-muted"><small>Nivel Superior</small></p>-->
                                                            </td>
                                                            <td>
                                                            <p class="mb-0 text-muted"></p>
                                                            
                                                            </td>
                                                            <td>
                                                            <p class="mb-0 text-muted">{{ $data->administrative_contact_name }}</p>
                                                            
                                                            </td>
                                                            <td>
                                                            <p class="mb-0 text-muted"><small>{{ $data->email }}</small></p> 
                                                            <td>
                                                            <p class="mb-0 text-muted">{{ $data->administrative_contact_phone }}</p>
                                                            </td>

                                                            <td>
                                                           
                                                            <p class="mb-0 text-muted">{{ $data->direction }}</p>
                                                            </td>

                                                            <td class="text-center">
                                                                <!-- Center modal -->
                                                                <button  type="button" class="btn-xs btn btn-warning waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center"><i class="mdi mdi-square-edit-outline"></i></button>
                                                            </td>
                                                            <td class="text-center">

                                                                <button  type="button" class="btn-xs btn btn-danger waves-effect waves-light" data-toggle="modal" data-target=".bs-modal-delete"><i class="mdi mdi-close"></i></button>

                                                            </td>

                                                    </tr>                                                   
                                                </tbody>
                                                @endif
                                            @endforeach                                    
                                        </table>     
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                            @foreach ($Providers as $data)
                                @if ($data != 'null')                                  
                            <form method="POST" action="{{ route('update.provider') }}">
                                @csrf
                                <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myCenterModalLabel">Editar Proveedor</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <p class="text-muted font-13 mt-3 mb-2 text-center">{{ $data->comercial_name }}</p>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                        <label for="fullname">Razón Social</label>
                                                        <input class="form-control" type="text" name="social_reason" id="social_reason" placeholder="{{ $data->social_reason }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="fullname">Nombre Comerial</label>
                                                    <input class="form-control" type="text" name="comercial_name" id="comercial_name" placeholder="{{ $data->comercial_name }}">
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="fullname">RUC</label>
                                                    <input class="form-control" type="text" name="ruc" id="ruc" placeholder="{{ $data->ruc }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="fullname">Dirección</label>
                                                    <input class="form-control" type="text" name="direction" id="direction" placeholder="{{ $data->direction }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="fullname">Nombre del Contacto Administrativo</label>
                                                    <input class="form-control" type="text" name="administrative_contact_name" id="administrative_contact_name" placeholder="{{ $data->administrative_contact_name }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="fullname">Teléfono del Contato Administrativo</label>
                                                    <input class="form-control" type="text" name="administrative_contact_phone" id="administrative_contact_phone" placeholder="{{ $data->administrative_contact_phone }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <label for="emailaddress">Email Comercial</label>
                                                    <input class="form-control" type="email" name="email" id="email" placeholder="{{ $data->email }}">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                <div class="radio radio-info form-check-inline">
                                                @if($data->status === 1)
                                                    <input type="radio" id="inlineRadio1" value="1" name="status" checked/>
                                                    <label for="inlineRadio1">Activa </label>
                                                @else
                                                    <input type="radio" id="inlineRadio1" value="1" name="status" />
                                                    <label for="inlineRadio1">Activa </label>
                                                @endif
                                                </div>
                                                <div class="radio radio-danger form-check-inline">
                                                @if($data->status === 0)
                                                    <input type="radio" id="inlineRadio2" value="0" name="status" checked>
                                                    <label for="inlineRadio2"> Cancelado </label>
                                                @else
                                                    <input type="radio" id="inlineRadio2" value="0" name="status" >
                                                    <label for="inlineRadio2"> Cancelado </label>
                                                @endif    
                                                </div>
                                                    </div>
                                                </div>
                                            </div>                                                                          
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                                                <input type="hidden" name="id" value="{{$data->id}}" />
                                                <button type="submit" class="btn btn-success">Guardar Cambios</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </form>
                            @endif
                        @endforeach
                        @foreach ($Providers as $data)
                                @if ($data != 'null')                                  
                            <form method="POST" action="{{ route('pending-user') }}">
                                @csrf
                                <div class="modal fade bs-modal-refuse" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myCenterModalLabel">Rehazar Contrato</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                               ¿ Esta Seguro que Desea Rehazar el Contrato ?                                          
                                            </div>                                       
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                                                <input type="hidden" name="id" value="{{$data->id}}" />
                                                <input type="hidden" name="contract" value="CANCEL" />
                                                <button type="submit" class="btn btn-danger">Rehazar Contrato</button>
                                            </div>

                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </form>
                            @endif
                        @endforeach
                        @foreach ($Providers as $data)
                                @if ($data != 'null')                                  
                            <form method="POST" action="{{ route('destroy.provider') }}">
                                @csrf
                                <div class="modal fade bs-modal-delete" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myCenterModalLabel">Anular Proveedor</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                               ¿ Esta Seguro que Desea Anular el Proveedor?                                          
                                            </div>                                       
                                            <div class="modal-footer">
                                                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                                                <input type="hidden" name="id" value="{{$data->id}}" />
                                                <button type="submit" class="btn btn-danger">Guardar Cambios</button>
                                            </div>

                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </form>
                            @endif
                        @endforeach   
                        @if(count($Providers)>5)     
                            </div> <!-- end row-->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-right">
                                            <ul class="pagination pagination-rounded justify-content-end">
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                                        <span aria-hidden="true">«</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                                        <span aria-hidden="true">»</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        @endif
            </div>
            @endrole
                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
          
@include('skeleton.footer')