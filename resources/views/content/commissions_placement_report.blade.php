
@include('skeleton.header_picker')


<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                <li class="breadcrumb-item active">Reporte Detallado de Comisión de Colocación</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Reporte Detallado de Comisión de Colocación</h4>
                    </div><!-- end page title -->
                </div>
            </div><!-- end row -->
            
            
        @if ($data !== 0 )    


  <!-- Users Section-->
  <div class="row">
                <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Datos de Red</h4>
                                        <div class="form-group mb-3">
                                        <label>Rango de Fechas</label>
                                        Data: <input id="Date_search" type="text" placeholder="Search by Date" /><br>   
                                        </div>
                                        </p>
                                       <table id="basic-datatable" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>ID del Usuario</th>
                                                <th>Nombre del Comisionista</th>
                                                <th>Nombre del que vendio</th>
                                                <th>Marca</th>
                                                <th>Producto</th>
                                                <th>Cod Grupo Familiar</th>
                                                <th>Número de Solicitud</th>
                                                <th>Valor de la Venta</th>
                                                <th>Fecha LIQ/RECP</th>
                                                <th>Fecha de la Solicitud</th>
                                                <th>Fecha del Archivo de Colocación</th>
                                                <th>Comision Fija</th>
                                                <th>Comision Variable</th>
                                                <th class="text-center">Valor de la Comisión</th>
                                            </tr>
                                            </thead>

                                            <tbody>  
                                              {{--{{dd($data)}}--}}
                                        @foreach ($data as $datas)  
                                                                                                                                  

                                            <tr>
                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">{{ $datas['commissionist_id'] }}</h5>
                                                    </td>
                                               
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['commissionist_name'] }}</small></p>
                                                        </td>
                                                        <td>
                                                             <p class="mb-0 text-muted"><small>{{ $datas['seller_name'] }}</small></p>
                                                        </td>

                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['provider_name'] }}</small></p>
                                                        </td>

                                                        <td>
                                                        <p class="mb-0 text-muted"><small>{{ $datas['product_name'] }}</small></p>
                                                        </td>

                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['grupo_familiar'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['numero_solicitud'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['monto'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['fecha_liq_recp'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['fecha_solicitud'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['fecha_importacion'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['monto_fijo'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['monto_porcentual'] }}</small></p>
                                                        </td>
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['calculo']  }}</small></p>
                                                        </td>
                                            </tr>     
                                        @endforeach    
                                            </tbody>
                                        </table>  
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
            </div> <!-- end row-->


        @else
            <h4 class="modal-title text-center" id="myCenterModalLabel">No hay Usuarios Registrados</h4>
        @endif 
        </div> <!-- container -->

    </div> <!-- content -->
            </div><!-- end wrapper -->
    </div><!-- end vue id -->                     
@include('skeleton.footer_pickers')
    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
minDateFilter = "";
maxDateFilter = "";
$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[8]).getTime();
      console.log('aData',aData._date);
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        //console.log('aData',aData._date);
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        //console.log('aData',aData._date);
        return false;
      }
    }

    return true;
  }
);
$(document).ready(function() {
  $("#Date_search").val("");
  
});

var table = $('#basic-datatable').DataTable( {
  deferRender: true, 
  "autoWidth": false,     
  "search": {
    "columns":([9]),
    "regex": true,
    "caseInsensitive": false,
  },});
  

$("#Date_search").daterangepicker({
  "locale": {
    "format": "YYYY-MM-DD",
    "separator": " to ",
    "applyLabel": "Apply",
    "cancelLabel": "Cancel",
    "fromLabel": "From",
    "toLabel": "To",
    "customRangeLabel": "Custom",
    "weekLabel": "W",
    "daysOfWeek": [
      "Su",
      "Mo",
      "Tu",
      "We",
      "Th",
      "Fr",
      "Sa"
    ],
    "monthNames": [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ],
    "firstDay": 1
  },
  "opens": "center",
}, function(start, end, label) {
  maxDateFilter = end;
  minDateFilter = start;
  console.log('MaxDate', maxDateFilter.val);
  console.log('MainDate',minDateFilter.val);
  table.draw();  
});
</script>
