@include('skeleton.header_picker')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                    <li class="breadcrumb-item active">Vincular contrato con vendedor</li>
                                </ol>
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <h4 class="page-title">Vincular contrato con vendedor</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->

                <div class="account-pages mt-5 mb-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="card">

                                    <div class="card-body p-4">
                                        
                                        <div class="text-center w-75 m-auto">
                                            <a href="index.html">
                                                <span><img src="assets/images/lifeconnections.png" alt="" height="130"></span>
                                            </a>
                                            <p class="text-muted mb-4 mt-3">Conectando con todos.</p>
                                        </div>

                                        <h5 class="auth-title">Formulario de Vinculacion Recaudo</h5>
                                       
                                        <form method="POST" action="{{ route('seller_collect_store') }}" id="form-ajax">
                                            @csrf
                                            <div class="form-group">

                                            <label for="fullname">Seleccione un Proveedor o Marca</label>
                                                <select class="selectpicker" data-plugin="customselect" name="provider" id="provider">
                                                    <option name="0" value="0">Seleccione un Proveedor</option>
                                                    @foreach ($Providers as $Pro)
                                                 
                                                    <option name="{{ $Pro->comercial_name }}" value="{{ $Pro->id }}">{{ $Pro->comercial_name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                            <label for="fullname">Seleccione Un Vendedor</label>
                                                <select id="seller" name="seller" data-style="btn-light" class="selectpicker" data-live-search="true">
                                                    
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Numero de Solicitud</label>
                                                <input class="form-control" type="text" name="application_number" id="application_number" placeholder="Numero de Solicitud" required>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Ingresar la Fecha de la Firma del Solicitud</label>
                                                    <input id="signature_date" name="signature_date" type="text" class="form-control" data-provide="datepicker" data-date-autoclose="true">
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Nombres del Cliente</label>
                                                <input class="form-control" type="text" name="client_name" id="administrative_contact_name" placeholder="Ingresar los Nombres del Cliente" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Apellidos del Cliente</label>
                                                <input class="form-control" type="text" name="client_surnames" id="client_surnames" placeholder="Ingresar los Apellidos del Cliente" required>
                                            </div>
                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-danger btn-block" type="submit"> Registrar</button>
                                            </div>

                                        </form>
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>


                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
</body>            
@include('skeleton.footer_pickers')

<script>
    $(document).ready(function (){
        $('#provider').on('change',function (){
            var provider_id = $(this).val();
            console.log(provider_id);
            if (provider_id) {
            $.ajax({
                url: "{{ route('seller_collect_list') }}",
                type: 'GET',
                data: { provider_id: provider_id },
                dataType: 'json',
                success: function (response) {
                    $('#seller').empty();
                    $('#seller').append("<option value='' >Seleccione Un Vendedor</option>");
                    $.each(response.data, function (key, value) {
                        console.log(value.recipient_fullname);                       
                        $('#seller').append("<option value='"+ value.recipient_user_id +"' >"+ value.recipient_fullname +"</option>");
                        $('#seller').selectpicker('refresh');

                    });
                },
                error : function(){
                    alert('Hubo un error obteniendo las los usuarios!');
                }
            });
        }
        });

    });
        
</script>