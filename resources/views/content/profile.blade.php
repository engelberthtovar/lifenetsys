@include('skeleton.header')
<body>
        <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        @include('topbar.topbar')
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
            @include('sidebar.sidebar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div class="container-fluid">
                    

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">LifeConSys</a></li>
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pagina</a></li>
                                        <li class="breadcrumb-item active">Mi Red</li>
                                    </ol>
                                </div>
                                <h4 class="page-title">LifeConSys</h4>
                            </div>
                        </div>
                    </div>     
                    <!-- end page title --> 
                </div> <!-- container -->

                
            
                <div class="row ">
                    <div class="col-lg-4" style="margin-left:30%">
                        <div class="text-center card-box">
                            <div class="pt-2 pb-2">
                                    <img src="https://ui-avatars.com/api/?name={{Auth::user()->name}}&amp;color=7F9CF5&amp;background=EBF4FF" class="rounded-circle img-thumbnail avatar-xl"
                                    alt="profile-image">
                                <h4 class="mt-3"><a href="extras-profile.html" class="text-dark">{{Auth::user()->name}}</a></h4>
                                <p class="text-muted"><i class="mdi mdi-map-marker-outline"></i> Lima Perù</p>

                                <div class="font-14">
                                <!--<span class="badge badge-light-danger badge-pill">Mapfre</span> -->
                                    <!--<span class="badge badge-light-secondary badge-pill">Comercio</span> -->
                                    <!--<span class="badge badge-light-success badge-pill">Oncosalud</span> -->
                                </div>

                                <p class="text-muted mt-3">hago cualquier cosa.</p>

                                <div class="row mt-3">
                                    <div class="col-4">
                                        <div class="mt-3">
                                            <h4>0%</h4>
                                            <p class="mb-0 text-muted text-truncate">Ventas</p>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mt-3">
                                            <h4>S./000</h4>
                                            <p class="mb-0 text-muted text-truncate">Comisiones</p>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mt-3">
                                            <h4>00</h4>
                                            <p class="mb-0 text-muted text-truncate">Total</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->

                            </div> <!-- end .padding -->
                        </div> <!-- end card-box-->
                    </div> <!-- end col -->
                </div> <!-- end row-->
                @if ($user['receiver_user_id'] != 0)
                <div class="row">
                    <div class="col-md-6 col-xl-4" style="margin-left:30%">
                        <div class="card-box widget-user">
                            <img src="https://ui-avatars.com/api/?name={{$user['leader_name']}}&amp;color=7F9CF5&amp;background=EBF4FF" class="img-responsive rounded-circle img-thumbnail" alt="user">
                            <h5 class="mb-1 text-center">Nivel Superior</h5>
                            <div class="mt-3">
                                <h5 class="mb-1">{{ $user['leader_name'] }}</h5>
                                <p class="text-muted mb-0 font-13">{{ $user['email'] }}</p>
                                <div class="user-position">
                                    <!--<span class="text-warning text-uppercase font-13 font-weight-bold">Nivel Superior</span>-->
                                </div>
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end col -->
                </div><!-- end row--> 
                @endif
            </div> <!-- container -->
        </div> <!-- content -->
    </div><!-- end wrapper -->
           
@include('skeleton.footer')