@include('skeleton.header')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                    <li class="breadcrumb-item active">Agregar Proveedor</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Agregar Proveedor</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->

                <div class="account-pages mt-5 mb-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="card">

                                    <div class="card-body p-4">
                                        
                                        <div class="text-center w-75 m-auto">
                                            <a href="index.html">
                                                <span><img src="{{ asset('assets/images/logo.png') }}" alt="" height="50"></span>
                                            </a>
                                            <p class="text-muted mb-4 mt-3">Conectando con todos.</p>
                                        </div>

                                        <h5 class="auth-title">Registrar Proveedor</h5>

                                        <form method="POST" action="{{ route('add.providers') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="fullname">Razón Social</label>
                                                <input class="form-control" type="text" name="social_reason" id="social_reason" placeholder="Razón Social" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Nombre Comerial</label>
                                                <input class="form-control" type="text" name="comercial_name" id="comercial_name" placeholder="Nombre Comerial" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">RUC</label>
                                                <input class="form-control" type="text" name="ruc" id="ruc" placeholder="RUC" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Dirección</label>
                                                <input class="form-control" type="text" name="direction" id="direction" placeholder="Dirección" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Nombre del Contacto Administrativo</label>
                                                <input class="form-control" type="text" name="administrative_contact_name" id="administrative_contact_name" placeholder="Nombre del Contacto Administrativo" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="fullname">Teléfono del Contato Administrativo</label>
                                                <input class="form-control" type="text" name="administrative_contact_phone" id="administrative_contact_phone" placeholder="Teléfono Administrativo - ej: +51 999999999" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="emailaddress">Email Comercial</label>
                                                <input class="form-control" type="email" name="email" id="email" required placeholder="Email Comercial">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailaddress">Cantidad de Usuarios Por Nivel</label>
                                                <input class="form-control" type="number" name="level_size" id="level_size" required placeholder="Cantidad de Usuarios Por Nivel">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailaddress">Cantidad De Niveles</label>
                                                <input class="form-control" type="number" name="size_depth" id="depth" required placeholder="Cantidad De Niveles">
                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox checkbox-info">
                                                    <input type="checkbox" class="custom-control-input" id="checkbox-signup">
                                                    <label class="custom-control-label" for="checkbox-signup"> Acepto <a href="javascript: void(0);" class="text-dark">Terminos y Condiciones</a></label>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-danger btn-block" type="submit"> Registrar</button>
                                            </div>

                                        </form>

                                    </div> <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>


                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
</body>            
@include('skeleton.footer')