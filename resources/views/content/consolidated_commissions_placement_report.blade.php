
@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                <li class="breadcrumb-item active">Reporte Consolidado de Comisión de Colocación</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Reporte Consolidado de Comisión de Colocación</h4>
                    </div><!-- end page title -->
                </div>
            </div><!-- end row -->
            
            
            
        @if ($data !== 0 )    


            <!-- Users Section-->
            <div class="row">
                <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Datos de Red</h4>
                                        <p class="text-muted font-13 mb-4">
                                        </p>
                                        <table id="datatable-buttons" class="table table-striped ">
                                            <thead>
                                            <tr>
                                                <th>ID del Usuario</th>
                                                <th>Nombre del Usuario</th>
                                                <th>Total a Comisionar</th>
                                                <th>total de Transacciones</th>
                                            </tr>
                                            </thead>

                                            <tbody>                                               
                                        @foreach ($data as $datas)  
                                                                                                                                  

                                            <tr>
                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">{{ $datas['commissionist_id'] }}</h5>
                                                    </td>
                                               
                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['commissionist_name'] }}</small></p>
                                                        </td>
                                                        <td>
                                                             <p class="mb-0 text-muted"><small>{{ $datas['suma'] }}</small></p>
                                                        </td>

                                                        <td>
                                                            <p class="mb-0 text-muted"><small>{{ $datas['count'] }}</small></p>
                                                        </td>
                                            </tr>     
                                        @endforeach    
                                            </tbody>
                                        </table>  
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
            </div> <!-- end row-->

        @else
            <h4 class="modal-title text-center" id="myCenterModalLabel">No hay Usuarios Registrados</h4>
        @endif 
        </div> <!-- container -->

    </div> <!-- content -->
            </div><!-- end wrapper -->
    </div><!-- end vue id -->            
@include('skeleton.footer')
