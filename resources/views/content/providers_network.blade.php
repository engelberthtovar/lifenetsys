
@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Proveedores</a></li>
                                <li class="breadcrumb-item active">Red de Proveedores</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Red de Proveedores</h4>
                    </div><!-- end page title -->
                </div>
            </div><!-- end row -->
                                         
    @if($data)            

                <!-- Users Section-->
                <div class="row">
                    <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="header-title">Datos de Red</h4>
                                            <p class="text-muted font-13 mb-4">
                                            </p>
                                            <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                                                <thead>
                                                <tr>
                                                    <th colspan="2">Miembro</th>
                                                    <th colspan="2">Nivel Superior</th>
                                                    <th>Contrato</th>
                                                    <!--<th>RUC - Nivel Superior</th>-->
                                                    <!--<th>RUC - Miembro</th>-->
                                                </tr>
                                                </thead>
                                        @foreach( $data as $network)                      
                                    @if($network->head_user_id != 1)   
                                                <tbody>                                               
                                                                                                                                    
                                                
                                                <tr>
                                                
                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{$network->recipient_fullname}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $network->recipient_fullname }}</h5>
                                                                <p class="mb-0 text-muted"><small>Miembro</small></p>
                                                            </td>

                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{$network->head_user_fullname}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $network->head_user_fullname }}</h5>
                                                                <p class="mb-0 text-muted"><small>Nivel Superior</small></p>
                                                            </td>
                                                    
                                                            <td> 
                                                                <span class="badge badge-light-success"> {{ $network->providers_comercial_name }}</span>
                                                            </td>

                                                            <!--<td>
                                                                {{ $network->providers_comercial_name }}
                                                            </td>

                                                            <td>
                                                                {{ $network->providers_comercial_name }}
                                                            </td>-->
                                                </tr>     
                                            
                                                </tbody>
                                                @endif 
                                            @endforeach   
                                            </table>  
                                        </div> <!-- end card body-->
                                    </div> <!-- end card -->
                                </div><!-- end col-->
                </div> <!-- end row-->

            @else
                <h4 class="modal-title text-center" id="myCenterModalLabel">No hay Usuarios Registrados</h4>
            @endif              
                @if(count($data)>5)
                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                <ul class="pagination pagination-rounded justify-content-end">
                                    <li class="page-item">
                                        <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                @endif 
        </div> <!-- container -->

    </div> <!-- content -->
            </div><!-- end wrapper -->
    </div><!-- end vue id -->            
            @include('skeleton.footer')
