
@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">LifeConSys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Páginas</a></li>
                                <li class="breadcrumb-item active">Usuarios</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Usuarios</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-8">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <label for="inputPassword2" class="sr-only">Buscar</label>
                                        <input type="search" class="form-control" id="inputPassword2"
                                            placeholder="Buscar...">
                                    </div>
                                </form>
                            </div>
                            </div><!-- end col-->
                        </div> <!-- end row -->
                    </div> <!-- end card-box -->
                </div><!-- end col-->
            </div>
            <!-- end row -->
            <!-- Users Section-->
            <div class="row">
                @foreach ($users as $user)  
                    @if ($user->email_verified_at != null)
                        @if ($user->id !== 1 &&  $user->id !== 2)
                            <div class="col-lg-4">
                                <div class="text-center card-box">
                                    <div class="pt-2 pb-2">
                                        
                                        @if ($user->profile_photo_path == null)
                                            <img src="https://ui-avatars.com/api/?name={{$user->name}}&amp;color=7F9CF5&amp;background=EBF4FF" class="rounded-circle img-thumbnail avatar-xl"
                                            alt="profile-image">
                                            
                                        @else
                                            <img src="storage/{{$user->profile_photo_path}}" class="rounded-circle img-thumbnail avatar-xl"
                                            alt="profile-image">
                                        @endif
                                        <h4 class="mt-3"><a href="extras-profile.html" class="text-dark">{{$user->name}}</a></h4>
                                        <p class="text-muted"><i class="mdi mdi-map-marker-outline"></i> Lima Perù</p>

                                        <div class="font-14">
                                            <!--<span class="badge badge-light-danger badge-pill">Mapfre</span>
                                            <span class="badge badge-light-secondary badge-pill">Comercio</span>
                                            <span class="badge badge-light-success badge-pill">Oncosalud</span>-->
                                        </div>

                                        <p class="text-muted mt-3">Emprendedor con, Conocimiento en el Area de Ventas.</p>

                                        <div class="row mt-3">
                                            <div class="col-4">
                                                <div class="mt-3">
                                                    <h4>0%</h4>
                                                    <p class="mb-0 text-muted text-truncate">Ventas</p>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mt-3">
                                                    <h4>S./000</h4>
                                                    <p class="mb-0 text-muted text-truncate">Comisiones</p>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mt-3">
                                                    <h4>000</h4>
                                                    <p class="mb-0 text-muted text-truncate">Total</p>
                                                </div>
                                            </div>
                                        </div> <!-- end row-->

                                    </div> <!-- end .padding -->
                                </div> <!-- end card-box-->
                            </div> <!-- end col -->
                        @endif
                    @endif    
                @endforeach
            </div> <!-- end row-->
               

                

            <div class="row">
                <div class="col-12">
                    <div class="text-right">
                        <ul class="pagination pagination-rounded justify-content-end">
                            <li class="page-item">
                                <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                            <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                            <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                            <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                            <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                            <li class="page-item">
                                <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div> <!-- container -->

    </div> <!-- content -->
            </div><!-- end wrapper -->
    </div><!-- end vue id -->
</body>            
            @include('skeleton.footer')
