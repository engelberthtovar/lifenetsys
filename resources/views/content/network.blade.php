
@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                <li class="breadcrumb-item active">Red</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Red</h4>
                    </div><!-- end page title -->
                </div>
            </div><!-- end row -->
            
            
        @if ($data !== 0 )    
            <!-- Users Section-->
            <div class="row">
                <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Datos de Red</h4>
                                        <p class="text-muted font-13 mb-4">
                                        </p>
                                        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Miembro</th>
                                                <th colspan="2">Nivel Superior</th>
                                                <th>Estado</th>
                                                <th>RUC - Nivel Superior</th>
                                                <th>RUC - Miembro</th>
                                                <th class="text-center">Documento</th>
                                            </tr>
                                            </thead>

                                            <tbody>                                               
                                        @foreach ($data as $user)                                                                                            
                                            @if ($user['sender']['0']->email_verified_at != null)
                                            <tr>
                                                @if ($user['sender']['0']->profile_photo_path == null)
                                                        <td style="width: 36px;">
                                                            <img src="https://ui-avatars.com/api/?name={{$user['invited']['0']->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                        </td>
                                                        <td>
                                                            <h5 class="m-0 font-weight-normal">{{ $user['invited']['0']->name }}</h5>
                                                            <p class="mb-0 text-muted"><small>Miembro</small></p>
                                                        </td>
                                                        @else
                                                        <td style="width: 36px;">
                                                            <img src="storage/{{$user['sender']['0']->profile_photo_path}}" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                        </td>
                                                        <td>
                                                            <h5 class="m-0 font-weight-normal">{{ $user['sender']['0']->name }}</h5>
                                                            <p class="mb-0 text-muted"><small>Nivel Superior</small></p>
                                                        </td>
                                                @endif 

                                                @if ($user['sender']['0']->profile_photo_path == null)
                                                        <td style="width: 36px;">
                                                            <img src="https://ui-avatars.com/api/?name={{$user['sender']['0']->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                        </td>
                                                        <td>
                                                            <h5 class="m-0 font-weight-normal">{{ $user['sender']['0']->name }}</h5>
                                                            <p class="mb-0 text-muted"><small>Nivel Superior</small></p>
                                                        </td>
                                                        @else
                                                        <td style="width: 36px;">
                                                            <img src="storage/{{$user['sender']['0']->profile_photo_path}}" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                        </td>
                                                        <td>
                                                            <h5 class="m-0 font-weight-normal">{{ $user['sender']['0']->name }}</h5>
                                                            <p class="mb-0 text-muted"><small>Nivel Superior</small></p>
                                                        </td>
                                                @endif 
                                                        <td>
                                                            @if($user['invited']['0']->contract == 'TRUE')
                                                            <span class="badge badge-light-success"> Aprobado</span>
                                                            @elseif ($user['invited']['0']->contract == 'CANCEL')
                                                            <span class="badge badge-light-danger">Rechazado</span>
                                                            @else  
                                                            <span class="badge badge-light-warning">Pendiente</span> 
                                                            @endif
                                                        </td>

                                                        <td>
                                                            {{ $user['sender']['0']->ruc }}
                                                        </td>

                                                        <td>
                                                        {{ $user['invited']['0']->ruc }}
                                                        </td>
                                                        <td>
                                                            @if ($user['contracts'] !== 0)
                                                                <button type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#myModal" disabled>
                                                                    <span class="btn-label" href=""><i class="fas fa-cloud-upload-alt"></i></span>Contrato Archivado
                                                                </button>
                                                            @else
                                                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#myModal" >
                                                                    <span class="btn-label" href=""><i class="fas fa-cloud-upload-alt"></i></span>Subir Contrato
                                                                </button>
                                                            @endif
                                                            </td>
                                            </tr>
                                            @endif      
                                        @endforeach    
                                            </tbody>
                                        </table>  
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
            </div> <!-- end row-->
            <form method="POST" action="{{ route('postul-upload') }}" enctype="multipart/form-data">    
                @csrf
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Importar Contrato</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <h6>Subir Contrato</h6>
                                        <p>Haz Click en el Boton "Seleccionar Archivo", busca con el nombre"Contrtato.pdf" seleccionalo y luego click en Subir Contrato .</p>
                                        <p>(El Archivo debe llamarse "Contrato" y ser un Archivo "PDF")</p>
                                        <hr>
                                            <div class="form-group">                                                                                                                   
                                                <input type="hidden" name="user_id" value="{{ $user['invited']['0']->id }}"/>
                                                {{-- <input type="hidden" name="provider_id" value="{{ $data['0']['provider']['0']->id }}"/> --}}
                                                <div class="row">
                                                    <div class="col-sm-4 ">
                                                    <div class="invoiceBox">
                                                    <label for="file">
                                                        <div class="boxFile" data-text="Seleccionar Archivo">
                                                        Seleccionar Archivo
                                                        </div>
                                                    </label>
                                                    <!--<input id="file" multiple="" name="file" type="file" size="600"  accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff">-->
                                                    <!--<input id="file" name="file" type="file" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" class="btn btn-purple btn-large"/>-->
                                                    <input id="file" name="file" type="file" accept="application/pdf" class="btn btn-purple btn-large" onchange="return validarExt()"/>
                                                        <br><br>
                                                        <div id="visorArchivo">
                                                            <!--Aqui se desplegará el fichero-->
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <!--<input id="file" name="file" type="file" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" class="btn btn-purple btn-large"/>
                                                <!--<button name="file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="btn btn-purple btn-large">Subir Contrato</button>-->
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <!--<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>-->
                                            <button type="submit" class="btn btn-success btn-block">Subir Contrato</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->  
                            </form>
            @if(count($data)>5)
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <ul class="pagination pagination-rounded justify-content-end">
                                <li class="page-item">
                                    <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                        <span aria-hidden="true">«</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                        <span aria-hidden="true">»</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            @endif
            <!-- Modal -->
            <div id="custom-modal" class="modal-demo">
                <button type="button" class="close" onclick="Custombox.modal.close();">
                    <span>&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="custom-modal-title">Add Members</h4>
                <div class="custom-modal-text text-left">
                    <form>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input type="text" class="form-control" id="location" placeholder="Enter location">
                        </div>
                        <div class="form-group">
                            <label for="skill">Skill</label>
                            <input type="text" class="form-control" id="skill" placeholder="Enter skill with comma">
                        </div>
                        <div class="form-group">
                            <label for="about-details">About </label>
                            <textarea class="form-control" id="about-details" rows="3" placeholder="Enter about"></textarea>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light m-l-10"
                                onclick="Custombox.modal.close();">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>

        @else
            <h4 class="modal-title text-center" id="myCenterModalLabel">No hay Usuarios Registrados</h4>
        @endif 
        </div> <!-- container -->

    </div> <!-- content -->
            </div><!-- end wrapper -->
    </div><!-- end vue id -->            
@include('skeleton.footer')

<script type="text/javascript">

function validarExt()
{
    var file = document.getElementById('file');
    var archivoRuta = file.value;
    var extPermitidas = /(.pdf|.PDF)$/i;
    if(!extPermitidas.exec(archivoRuta)){
        alert('Asegurese de haber seleccionado un PDF');
        file.value = '';
        return false;
    }

    else
    {
        //Previo del PDF
        if (file.files && file.files[0]) 
        {
            var visor = new FileReader();
            visor.onload = function(e) 
            {
                document.getElementById('visorArchivo').innerHTML = 
                '<embed src="'+e.target.result+'" width="465" height="375" />';
            };
            visor.readAsDataURL(file.files[0]);
        }
    }
}
</script>