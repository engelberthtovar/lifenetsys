@include('skeleton.header')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                    <li class="breadcrumb-item active">Crear Role</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Crear Role</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->

                <div class="account-pages mt-5 mb-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="card">

                                    <div class="card-body p-4">
                                        
                                        <div class="text-center w-75 m-auto">
                                            <a href="index.html">
                                                <span><img src="{{ asset('assets/images/logo.png') }}" alt="" height="50"></span>
                                            </a>
                                            <p class="text-muted mb-4 mt-3">Conectando con todos.</p>
                                        </div>

                                        <h5 class="auth-title">Crear Role</h5>

                                        <form method="POST" action="{{ route('Roles') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="fullname">Nombre Del Role</label>
                                                <input class="form-control" type="text" name="create_role" id="create_role" placeholder="Nombre Del Role" required>
                                            </div>

                                            </div>
                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-danger btn-block" type="submit"> Crear</button>
                                            </div>

                                        </form>

                                    </div> <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>


                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
</body>            
@include('skeleton.footer')