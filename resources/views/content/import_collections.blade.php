@include('skeleton.header')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
            <!-- Begin page -->
        <div id="wrapper">
                <!-- Topbar Start -->
                    @include('topbar.topbar')
                <!-- end Topbar -->
                <!-- ========== Left Sidebar Start ========== -->
                    @include('sidebar.sidebar')
                <!-- Left Sidebar End -->
            <div class="content-page">
                <div class="content">
                        <!-- Start Content-->
                    <div class="container-fluid">    
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Proveedores</a></li>
                                            <li class="breadcrumb-item active">Cargar Trama Recaudo</li>
                                        </ol>
                                        @include('alerts.success')
                                        @include('alerts.errors')  
                                    </div>
                                    <h4 class="page-title">Cargar Trama Recaudo</h4>
                                </div> 
                            </div><!-- end page title -->
                        </div><!-- end row -->
                    </div> <!-- container -->




                    <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title m-t-0">Carga de archivos </h4>
                                       <!-- <p class="text-muted font-13 m-b-30">
                                            DropzoneJS is an open source library that provides drag’n’drop file uploads with image previews.
                                        </p>-->
    
                                        <!--<form action="{{ route('import_placement') }}" method="post" class="dropzone" id="myAwesomeDropzone" data-plugin="dropzone" data-previews-container="#file-previews" data-upload-preview-template="#uploadPreviewTemplate" enctype="multipart/form-data">-->
                                        <form action="{{ route('import_collections') }}" method="post" class="dropzone"  enctype="multipart/form-data">
                                            @csrf
                                            @if(Session::has('message'))
                                            <p>{{ Session::get('message') }}</p>
                                            @endif
                                            
                                            <div class="dz-message needsclick">
                                                <p class="h1 text-muted"><i class="mdi mdi-cloud-upload"></i></p>
                                               <!-- <h3>Suelta los archivos aquí o haz clic para subirlos.</h3>
                                                <span class="text-muted font-13">(This is just a demo dropzone. Selected files are
                                                    <strong>not</strong> actually uploaded.)</span>-->
                                                </div>
                                            <div class="fallback">
                                                <input name="file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
                                                <div class="form-group">
                                                    <br>
                                                    <button type="submit">Importar Data</button>
                                                <!--@error('file')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror-->
                                                </div>
                                            </div>
                                        </form>
    
                                        <!-- Preview -->
                                        <div class="dropzone-previews mt-3" id="file-previews"></div>
                                    </div>
                                    <!-- end card-body -->
                                </div>
                                <!-- end card-->
                            </div>
                            <!-- end col-->
                        </div>
                        <!-- end row -->

                        <!-- file preview template -->
                        <div class="d-none" id="uploadPreviewTemplate">
                            <div class="card mt-1 mb-0 shadow-none border">
                                <div class="p-2">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <img data-dz-thumbnail class="avatar-sm rounded bg-light" alt="">
                                        </div>
                                        <div class="col pl-0">
                                            <a href="javascript:void(0);" class="text-muted font-weight-bold" data-dz-name></a>
                                            <p class="mb-0" data-dz-size></p>
                                        </div>
                                        <div class="col-auto">
                                            <!-- Button -->
                                            <a href="" class="btn btn-link btn-lg text-muted" data-dz-remove>
                                                <i class="mdi mdi-close-circle"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>







                </div> <!-- content-->
            </div> <!-- content page-->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->    

                @include('skeleton.footer_fileuploads')
                <script type="text/javascript">
                    Dropzone.options.myAwesomeDropzone = {
                        autoProcessQueue : false,
                        acceptedFiles : ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
                        headers:{
                            'X-CSRF-TOKEN' : "{{ csrf_token() }}"
                        }
                    };
                </script>