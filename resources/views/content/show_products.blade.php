@include('skeleton.header')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                    <li class="breadcrumb-item active">Listar Productos</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Listar Productos</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->


                @if($Provider !== 0 )    
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title">Listado de Productos</h4>
                                    <p class="text-muted font-13 mb-4">
                                    </p>
                                                                        
                                    <table id="datatable" class="table table-striped dt-responsive nowrap">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><small>Nombre del Proveedor</small></th>
                                            <th class="text-center"><small>Nombre del Producto</small></th>
                                            <th class="text-center"><small>Descripción</small></th>
                                            <th class="text-center"><small>Valor Referencial</small></th>
                                            <th class="text-center"><small>Tipo de Comisión</small></th>
                                            <th class="text-center"><small>Tipo de Comisión</small></th>
                                            <th class="text-center"><small>Comisiones</small></th>
                                        </tr>
                                        </thead>
                                    
                                    @foreach ($Provider as $Providers)
                                        @foreach ($Products as $data)                                         
                                            @if ($data !== 'null')                                          
                                                <tbody>                                                
                                                    <tr>
                                                        <td>
                                                            <div class="row justify-content-center">
                                                                <h5 class="m-0 font-weight-normal">{{ $Providers->comercial_name }}</h5>
                                                            </div>
                                                        </td>
                                                        <td>
                                                             <div class="row justify-content-center">
                                                                <h5 class="m-0 font-weight-normal">{{ $data->product_name }}</h5>
                                                            <!--<p class="mb-0 text-muted"><small>Nivel Superior</small></p>-->
                                                            </div> 
                                                        </td>
                                                        <td>
                                                            <div class="row justify-content-center">
                                                                <p class="mb-0 text-muted"><small>{{ $data->description }}</small></p>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <div class="row justify-content-center">
                                                            <p class="mb-0 text-muted">{{ $data->referencial_value }}</p>
                                                        </div> 
                                                        </td>
                                                        @if ($data->colocacion !== null) 
                                                            <td>
                                                            <div class="row justify-content-center">
                                                                <p class="mb-0 text-muted">Colocación</p> 
                                                            </div> 
                                                            </td>
                                                        @else
                                                            <td>
                                                            <div class="row justify-content-center">
                                                                <p class="mb-0 text-muted">No Posee Comision de Colocación</p> 
                                                            </div> 
                                                            </td>
                                                        @endif
                                                        @if ($data->recaudo !== null) 
                                                            <td>
                                                            <div class="row justify-content-center">
                                                                <p class="mb-0 text-muted">Recaudo</p> 
                                                            </div> 
                                                            </td>
                                                            @else
                                                            <td>
                                                            <div class="row justify-content-center">
                                                                <p class="mb-0 text-muted">No Posee Comision de Recaudo</p>
                                                            </div> 
                                                            </td>
                                                        @endif
                                                        <td>
                                                            <div class="form-group text-center">
                                                            @foreach ($CommissionValue as $Commission)
                                                               
                                                                <button id="ver" type="button" class="btn btn-success waves-effect waves-light"   data-nivel="{{ $Commission->nivel }}" data-colocacion="{{ $Commission->colocacion .' '. $data->calculo_colocacion }}" data-recaudo="{{ $Commission->recaudo .' '. $data->calculo_recaudo }}" data-product_name="{{ $data->product_name }}" data-toggle="modal" data-target=".bs-example-modal-center"><i class="fas fa-eye"></i></button>
                                                            @endforeach
                                                            </div>  
                                                        </td>
                                                </tr>                                                   
                                            </tbody>
                                            @endif
                                        @endforeach
                                    @endforeach
     
                                    </table>     
                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                        <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myCenterModalLabel">Comisiones</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-muted font-13 mt-3 mb-2 text-center">Valores de las Comisiones</p>                                          
                                            <div class="table-responsive">
                                                <table class="table table-bordered mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th>Nombre del Producto</th>
                                                        <th>Nivel de Comisión</th>
                                                        <th>Monto de la Comisión de Colocación</th>
                                                        <th>Monto de la Comisión de Recaudo</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr id="modal">
  
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                                            
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
    
                                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->  
                    @else                                   
                        <h4 class="modal-title text-center">No hay Productos Registrados</h4>
                    @endif
                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
</body>            
@include('skeleton.footer')

<script>

$('button[id=ver]').on('click',function () {
    $("#modal").html('');
    nivel = $(this).data("nivel");
    colocacion = $(this).data("colocacion");
    recaudo = $(this).data("recaudo");
    product_name = $(this).data("product_name");
    console.log('product_name',product_name);
    console.log('recaudo',recaudo)
    console.log('colocacion',colocacion)
    console.log('nivel',nivel)
  //alert($(this).data("id"));
  innerhtml = '';
  innerhtml += "<th scope='row'>"+product_name+"</th>";
  innerhtml += "<td>"+nivel+"</td>" ;
  innerhtml += "<td>"+colocacion+"</td>" ;
  innerhtml += "<td>"+recaudo+"</td>" ;
  //innerhtml += "<input type='hidden' name='user' value='"+id+"'/>" ;




  $('#modal').append(innerhtml);
  innerhtml = '';
});

</script>