@include('skeleton.header_picker')
<body class="authentication-bg authentication-bg-pattern">
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
            <div class="container-fluid">    
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                    <li class="breadcrumb-item active">Buscar  contrato con vendedor</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Buscar contrato con vendedor</h4>
                        </div> 
                    </div><!-- end page title -->
                </div><!-- end row -->

                <div class="account-pages mt-5 mb-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-5">
                                <div class="card">

                                    <div class="card-body p-4">
                                        
                                        <div class="text-center w-75 m-auto">
                                            <a href="index.html">
                                                <span><img src="assets/images/lifeconnections.png" alt="" height="130"></span>
                                            </a>
                                            <p class="text-muted mb-4 mt-3">Conectando con todos.</p>
                                        </div>

                                        <h5 class="auth-title">Buscar Vendedor</h5>
                                       
                                        <form method="POST" action="{{ route('sellerStore') }}" id="form-ajax">
                                            @csrf
                                            <div class="form-group">
                                            
                                            <label for="fullname">Seleccione o coloque un Numero de Soliitud</label>
                                                <select class="selectpicker" data-plugin="customselect" name="application" id="application" data-live-search="true">
                                                    <option name="0" value="0">Seleccione un Numero de Soliitud</option>
                                                    @foreach ($PlacementWorkingTable as $Placement)
                                                    <option name="{{ $Placement->nro_solicitudnro_solicitud }}" value="{{ $Placement->nro_solicitud }}">{{ $Placement->nro_solicitud }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                            <label for="fullname">Seleccione Un Vendedor</label>
                                                <select id="seller" name="seller" data-style="btn-light" class="selectpicker" data-live-search="false">

                                                </select>
                                            </div>
                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-danger btn-block" type="submit"> Registrar</button>
                                            </div>

                                        </form>
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>


                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
</body>            
@include('skeleton.footer_pickers')

<script>
    $(document).ready(function (){
        $('#application').on('change',function (){
            var application_number = $(this).val();
            console.log(application_number);
            if (application_number) {
            $.ajax({
                url: "{{ route('seller_list') }}",
                type: 'GET',
                data: { application_number: application_number },
                dataType: 'json',
                success: function (response) {
                    $('#seller').empty();
                    $.each(response.data, function (key, value) {
                        console.log(value.recipient_fullname);
                        $('#seller').append("<option value='' >Seleccione Un Vendedor</option>");
                        $('#seller').append("<option value='"+ value.id +"' >"+ value.name +"</option>");
                        $('#seller').selectpicker('refresh');

                    });
                },
                error : function(){
                    alert('No hay ningun Vendedor Asociado a ese Numero de Solicitud !');
                }
            });
        }
        });

    });
        
</script>