@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
        <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
                <!-- end Topbar -->
                <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
                <!-- Left Sidebar End -->
            <div class="content-page">
                <div class="content">
             <!-- Start Content-->
                    <div class="container-fluid">    
                        <!-- start page title -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box">
                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                                <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                                <li class="breadcrumb-item active">Red De Usuarios</li>
                                            </ol>
                                        </div>
                                        <h4 class="page-title">Red De Usuarios</h4>
                                    </div> 
                                </div><!-- end page title -->
                            </div><!-- end row -->
                         
                        @foreach( $data as $network)                       
                            @if($network->head_user_id != 1)
                            
                                <div class="body genealogy-body genealogy-scroll">
                                    <div class="genealogy-tree">
                                        <!--<ul class="active">-->
                                        <ul class="active">
                                            <li>
                                                <a href="javascript:void(0);">
                                                    <div class="member-view-box">
                                                        <div class="member-image">
                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                            <div class="member-details">
                                                                <h5>{{ $network->head_user_fullname}}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <!-------------- Vendedor 1 --------------->
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <div class="member-view-box">
                                                                <div class="member-image">
                                                                    <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                    <div class="member-details">
                                                                        <h5 class="text-center">{{ $network->recipient_fullname }}</h5>
                                                                        <h5 class="text-center"> (1)</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        
                                                        <ul>
                                                            
                                                            <li>
                                                                <a href="javascript:void(0);">
                                                                    <div class="member-view-box">
                                                                        <div class="member-image">
                                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                            <div class="member-details">
                                                                                <h5>Vendedor</h5>
                                                                                <h5> (1.1)</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="javascript:void(0);">
                                                                                <div class="member-view-box">
                                                                                    <div class="member-image">
                                                                                        <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                        <div class="member-details">
                                                                                            <h5>Vendedor</h5>
                                                                                            <h5>(1.1.1)</h5>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:void(0);">
                                                                                    <div class="member-view-box">
                                                                                        <div class="member-image">
                                                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                            <div class="member-details">
                                                                                                <h5>Vendedor</h5>
                                                                                                <h5>(1.1.2)</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                        </li>
                                                                    </ul>
                                                            </li>


                                                            <li>
                                                                <a href="javascript:void(0);">
                                                                    <div class="member-view-box">
                                                                        <div class="member-image">
                                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                            <div class="member-details">
                                                                                <h5>Vendedor</h5>
                                                                                <h5> (1.2)</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>

                                                                    <ul>
                                                                        <li>
                                                                            <a href="javascript:void(0);">
                                                                                <div class="member-view-box">
                                                                                    <div class="member-image">
                                                                                        <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                        <div class="member-details">
                                                                                            <h5>Vendedor</h5>
                                                                                            <h5>(1.2.1)</h5>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:void(0);">
                                                                                    <div class="member-view-box">
                                                                                        <div class="member-image">
                                                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                            <div class="member-details">
                                                                                                <h5>Vendedor</h5>
                                                                                                <h5>(1.2.2)</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                        </li>
                                                                    </ul>

                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <!-------------- Vendedor 2 --------------->
                                                    
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <div class="member-view-box">
                                                                <div class="member-image">
                                                                    <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                    <div class="member-details">
                                                                        <h5>Vendedor</h5>
                                                                        <h5> (2)</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <!--------------  --------------->
                                                        <ul>
                                                            <li>
                                                                <a href="javascript:void(0);">
                                                                    <div class="member-view-box">
                                                                        <div class="member-image">
                                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                            <div class="member-details">
                                                                                <h5>Vendedor</h5>
                                                                                <h5>(2.1)</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            <ul>
                                                                <li>
                                                                    <a href="javascript:void(0);">
                                                                        <div class="member-view-box">
                                                                            <div class="member-image">
                                                                                <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                <div class="member-details">
                                                                                    <h5>Vendedor</h5>
                                                                                    <h5>(2.1.1)</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:void(0);">
                                                                            <div class="member-view-box">
                                                                                <div class="member-image">
                                                                                    <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                    <div class="member-details">
                                                                                        <h5>Vendedor</h5>
                                                                                        <h5>(2.1.2)</h5>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                </li>
                                                            </ul>
                                                            
                                                            <!--------------  --------------->
                                                            
                                                            <li>
                                                                <a href="javascript:void(0);">
                                                                    <div class="member-view-box">
                                                                        <div class="member-image">
                                                                            <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                            <div class="member-details">
                                                                                <h5>Vendedor</h5>
                                                                                <h5>(2.2)</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                                <ul>
                                                                    <li>
                                                                        <a href="javascript:void(0);">
                                                                            <div class="member-view-box">
                                                                                <div class="member-image">
                                                                                    <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                    <div class="member-details">
                                                                                        <h5>Vendedor</h5>
                                                                                        <h5>(2.2.1)</h5>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:void(0);">
                                                                            <div class="member-view-box">
                                                                                <div class="member-image">
                                                                                    <img src="https://image.flaticon.com/icons/svg/145/145867.svg" alt="Member">
                                                                                    <div class="member-details">
                                                                                        <h5>Vendedor</h5>
                                                                                        <h5>(2.2.2)</h5>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                            @endif
                        @endforeach          


                </div> <!-- container -->
            </div> <!-- content -->
        </div><!-- end wrapper -->
    </div><!-- end vue id -->
    



          
@include('skeleton.footer')