
@include('skeleton.header')
<body>
    <div id="app"><!-- vue id -->
    <!-- Begin page -->
            <div id="wrapper">
            <!-- Topbar Start -->
                @include('topbar.topbar')
            <!-- end Topbar -->
            <!-- ========== Left Sidebar Start ========== -->
                @include('sidebar.sidebar')
            <!-- Left Sidebar End -->
            <div class="content-page">
            <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">    
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Lifeconsys</a></li>
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                                <li class="breadcrumb-item active">Usuarios Pendientes</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Usuarios Pendientes</h4>
                    </div> 
                </div><!-- end page title -->
            </div><!-- end row -->
            
            
        @if ($data !== 0 )     
            <!-- Users Section-->
            <div class="row">   
                <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Usuarios Pendientes</h4>
                                        <p class="text-muted font-13 mb-4">
                                        </p>
                                        
                                    
                                        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Postulante</th>
                                                <th colspan="2">Nivel Superior</th>
                                                <th>Estado</th>
                                                <th>RUC - Postulante</th>
                                                <th>Proveedor</th>
                                                <th class="text-center">Documento</th>
                                                <th>Aprobar Postulación</th>
                                                <th>Rechazar Postulación</th>
                                            </tr>
                                            </thead>
                                            
                                            @foreach ($data as $user)  
                                                                                      
                                                @if ($user['recipient_user'] !== 'null')
                                                
                                                    <tbody>                                                
                                                        <tr>
                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{$user['recipient_user']['0']->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $user['recipient_user']['0']->name }}</h5>
                                                                <p class="mb-0 text-muted"><small>Postulante</small></p>
                                                            </td>
                                                            <td style="width: 36px;">
                                                                <img src="https://ui-avatars.com/api/?name={{$user['head_user']['0']->name}}&amp;color=7F9CF5&amp;background=EBF4FF" alt="contact-img" title="contact-img" class="rounded-circle avatar-sm">
                                                            </td>
                                                            <td>
                                                                <h5 class="m-0 font-weight-normal">{{ $user['head_user']['0']->name }}</h5>
                                                                <p class="mb-0 text-muted"><small>Nivel Superior</small></p>
                                                            </td>
                                                            <td>
                                                                @if($user['post']->status == 'TRUE')
                                                                <span class="badge badge-light-success"> Firmado</span>
                                                                @elseif ($user['post']->status == 'CANCEL')
                                                                <span class="badge badge-light-danger">Rechazado</span>
                                                                @else  
                                                                <span class="badge badge-light-warning">Pendiente</span> 
                                                                @endif
                                                            </td>

                                                            <td>
                                                            {{ $user['recipient_user']['0']->ruc }}
                                                            </td>

                                                            <td>
                                                                {{ $user['provider']['0']->comercial_name }}
                                                            </td>
                                                            <td>
                                                                <!-- Download Contract Document -->
                                                                <!--<form method="POST" action="{{ route('postul-download') }}">
                                                                    @csrf
                                                                    <input type="hidden" name="user_id" value="{{$user['recipient_user']['0']->id}}" />
                                                                    <!--aqui va providers_contracts dentro del value
                                                                    <input type="hidden" name="document" value="{{$user['recipient_user']['0']->document}}" />
                                                                    <button type="submit" class="btn btn-success waves-effect waves-light">
                                                                        <span class="btn-label" href=""><i class=" fas fa-cloud-download-alt"></i></span>Descargar
                                                                    </button>
                                                                </form> -->
                                                                <input id="file" type="button" name="file"  class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#myModal" value="Vista Previa del Contrato" onchange="return validarExt()"/>
                                                                    <!--<span class="btn-label" href=""><i class="fas fa-cloud-upload-alt"></i></span>Contrato
                                                                <!--</button>-->


                                                            </td>
                                                            <td>
                                                                <!-- Center modal -->
                                                                <button  type="button" class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center">Confirmar</button>

                                                            </td>
                                                            <td>
                                                                <!-- Center modal -->
                                                                <button  type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#RefusedModal">Rechazar</button>

                                                            </td>

                                                    </tr>                                                   
                                                </tbody>
                                                @endif
                                            @endforeach                                    
                                        </table>     
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->

                            
                            <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Contrato de {{ $user['recipient_user']['0']->name }}</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                     
                                    @if($user['providers_contracts']->isNotEmpty())    
                                        <h6>Vista Previa del Contrato</h6>                                      
                                        <embed  src="{{ Storage::url('upload/' .$user['providers_contracts']['0']->document) }}"width="465" height="375"">                                   
                                        <div class="modal-footer">
                                            <!--<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>-->
                                            <button type="button" class="btn btn-success btn-block" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                    @else
                                    <h6>No hay ningun Contrato Adjuntado a este Usuario</h6> 
                                    @endif
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->  
                            </form>


                        @if(count($data)>5)     
                            </div> <!-- end row-->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-right">
                                            <ul class="pagination pagination-rounded justify-content-end">
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                                        <span aria-hidden="true">«</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                                <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                                        <span aria-hidden="true">»</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        @endif
            </div>
            <!-- end row -->

            @foreach ($data as $user)
                                @if ($user['recipient_user'] != 'null')      
                        
                            <form method="POST" action="{{ route('postul-refuse') }}">
                                @csrf
                            <div class="modal fade" id="RefusedModal" tabindex="-1" role="dialog" aria-labelledby="CenterModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="CenterModalLabel">Rehazar Contrato</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                        ¿ Esta seguro que Desea Rehazar este Contrato ?                                          
                                        </div>                                       
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                                            <input type="hidden" name="user_id" value="{{$user['recipient_user']['0']->id}}" />
                                            <input type="hidden" name="contract" value="CANCEL" />
                                            <button type="submit" class="btn btn-danger">Rehazar Contrato</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            </form>
                            @endif
                        @endforeach  







        @else
            <h4 class="modal-title text-center">No hay Usuarios Pendientes</h4>
        @endif 
        </div> <!-- container -->
  
                </div> <!-- content -->
        </div><!-- end wrapper -->
 
    </div><!-- end vue id -->
            <!-- end row -->
        @if ($data !== 0 ) 
            @foreach ($data as $user)
                @if ($user['recipient_user'] != 'null')                                  
            <form method="POST" action="{{ route('postul-update') }}">
                @csrf
            <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myCenterModalLabel">Aprobar Contrato</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                        <p class="text-muted font-13 mt-3 mb-2 text-center">Proveedores</p>
                            <div class="text-center">
                            <p class="text-muted font-13 mt-3 mb-2 text-center">¿Esta seguro que Desea Confirmar el Contrato ?</p>
                            </div>                                                                                   
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                            <input type="hidden" name="user_id" value="{{$user['recipient_user']['0']->id}}" />
                            <input type="hidden" name="user_name" value="{{$user['recipient_user']['0']->name}}" />
                            <input type="hidden" name="provider_id" value="{{$user['provider']['0']->id}}" />
                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->  
            </form>
            @endif
        @endforeach
        @else
            <h4 class="modal-title text-center"></h4>
        @endif  
@include('skeleton.footer')
<script type="text/javascript">
function validarExt()
{   
    alert('estas activando el botn');
   var file = document.getElementById('file');
    console.log(file);
    var visor = new FileReader();
        visor.onload = function(e) 
        {
            document.getElementById('visorArchivo').innerHTML = 
            '<embed src="'+e.target.result+'" width="465" height="375" />';
        };
}
</script>