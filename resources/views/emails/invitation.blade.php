@component('mail::message')
# El Usuario con el Nombre: {{ $data['name'] }} Te ha enviado una invitación para la aplicación Lifeconnections

Haga click en el botón para registrarse


  

@component('mail::button', ['url' => $data['url']])
Registrar
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
