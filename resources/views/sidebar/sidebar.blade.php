<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                @role('super_admin')
                <li class="menu-title">Administrador</li>               
                <li>
                    <a href="{{ route('dashboard') }}">
                        <i class="la la-dashboard"></i>
                        <span> Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin-user') }}">
                        <i class="mdi mdi-account-multiple-outline"></i>
                        <span> Usuarios </span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="mdi mdi-google-assistant"></i>
                        <span> LifeConsys </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{ route('show-user') }}">
                                <i class=" la la-user-times"></i>
                                <span> Usuarios pendientes </span>
                            </a>
                        </li>
                        <li>
                        <a href="{{ route('network') }}">
                            <i class=" la la-sitemap"></i>
                            <span> Red </span>
                        </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="la la-briefcase"></i>
                        <span> Proveedores </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                         <li>
                            <a href="{{ route('providers') }}">
                                <i class=" la la-list"></i>
                                <span> Lista de Marcas </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('show_products') }}">
                                <i class=" la la-list"></i>
                                <span> Lista de Productos </span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ route('providers.form') }}">
                                <i class=" la la-plus-square"></i>
                                <span> Agregar Marca </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('add_products') }}">
                                <i class=" la la-plus-square"></i>
                                <span> Agregar Producto </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('upload_placement') }}">
                                <i class=" fas fa-upload "></i>
                                <span> Cargar Colocación </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('upload_collections') }}">
                                <i class=" fas fa-upload "></i>
                                <span> Cargar Recaudo </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('prouserspen') }}">
                                <i class=" la la-user-times"></i>
                                <span> Usuarios pendientes </span>
                            </a>
                        </li>
                        <li>
                        <a href="{{ route('pronetwork') }}">
                            <i class=" la la-sitemap"></i>
                            <span> Red </span>
                        </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-cogs"></i>
                        <span> Utilidades </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{ route('seller_placement_show') }}">
                                <i class=" fas fa-link "></i>
                                <span> Vincular contrato con Vendedor </span>
                            </a>
                        </li>
                <!--    <li>
                            <a href="{{ route('seller_collect_show') }}">
                                <i class=" fas fa-link "></i>
                                <span> Vincular contrato Recaudo </span>
                            </a>
                        </li>   -->
                <!--    <li>
                            <a href="{{ route('migrate_placement') }}">
                                <i class=" fas fa-file-export"></i>
                                <span> {{ __('Migrar Datos de Colocación') }} </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('migrate_colletions') }}">
                                <i class=" fas fa-file-export"></i>
                                <span> {{ __('Migrar Datos de Recaudo') }} </span>
                            </a>
                        </li>   -->
                    <!--    <li>
                            <a href="{{ route('search_seller') }}">
                                <i class=" la la-list"></i>
                                <span> {{ __('Buscar Vendedor') }} </span>
                            </a>
                        </li>   -->
                        <li>
                            <a href="#" aria-expanded="false">
                                <i class=" la la-list"></i>
                                {{ __('Procesar') }}
                                        &nbsp;
                                {{ __('Comisiones') }}      
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-third-level nav" aria-expanded="false">
                                <li>
                                    <a href="{{ route('process_request_placement') }}">
                                        <i class=" la la-rotate-right"></i>
                                        <span>{{ __('Colocación') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('process_request_collect') }}">
                                        <i class="la la-rotate-left"></i>
                                        <span>{{ __('Recaudo') }}</span>
                                      </a>
                                </li>
                            </ul>
                        </li>
                <!--    <li>
                            <a href="#" aria-expanded="false">
                                <i class=" la la-list"></i>
                                {{ __('Calcular') }}
                                        &nbsp;
                                        &nbsp;
                                {{ __('Comisiones') }}      
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-third-level nav" aria-expanded="false">
                                <li>
                                    <a href="{{ route('calculate_placement') }}">
                                        <i class=" la la-rotate-right"></i>
                                        <span>{{ __('Colocación') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('calculate_collect') }}">
                                        <i class="la la-rotate-left"></i>
                                        <span>{{ __('Recaudo') }}</span>
                                      </a>
                                </li>
                            </ul>
                        </li>   -->
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-file-signature"></i>
                        <span> Reportes </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{ route('commissions_placement_report') }}">
                                <i class="fas fa-layer-group"></i>
                                <span> Reporte Detallado de Comisión de Colocación </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('consolidated_commissions_placement_report') }}">
                                <i class="fas fa-layer-group"></i>
                                <span> Reporte Consolidado de Comisión de Colocación </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('commissions_collect_report') }}">
                                <i class="fas fa-layer-group"></i>
                                <span> Reporte Detallado de Comisión de Recaudo </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('consolidated_commissions_collect_report') }}">
                                <i class="fas fa-layer-group"></i>
                                <span> Reporte Consolidado de Comisión de Recaudo </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <hr>
                @endrole
                <li class="menu-title">Miembro</li>                
                <li>
                    <a href="{{ route('welcome') }}">
                        <i class="la la-dashboard"></i>
                        <span> Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('view-profile',  Auth::user()->id ) }}">
                        <i class="mdi mdi-account-multiple-outline"></i>
                        <span> Usuarios </span>
                    </a>
                </li>
                @if(Auth::user()->contract != 'FALSE')
                <li>
                    <a href="{{ route('providers') }}">
                        <i class=" la la-list"></i>
                        <span> Lista de Marcas </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('view-profile',  Auth::user()->id ) }}">
                        <i class=" la la-sitemap"></i>
                        <span> Mi Red </span>
                    </a>
                </li>
                 <!--<li class="menu-title mt-2">Components</li>
                <li>
               <!-- <li>
                    <a href="javascript: void(0);">
                        <i class="la la-briefcase"></i>
                        <span> UI Elements </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="ui-buttons.html">Buttons</a>
                        </li>
                        <li>
                            <a href="ui-cards.html">Cards</a>
                        </li>
                        <li>
                            <a href="ui-tabs-accordions.html">Tabs & Accordions</a>
                        </li>
                        <li>
                            <a href="ui-modals.html">Modals</a>
                        </li>
                        <li>
                            <a href="ui-progress.html">Progress</a>
                        </li>
                        <li>
                            <a href="ui-notifications.html">Notifications</a>
                        </li>
                        <li>
                            <a href="ui-general.html">General UI</a>
                        </li>
                        <li>
                            <a href="ui-typography.html">Typography</a>
                        </li>
                        <li>
                            <a href="ui-grid.html">Grid</a>
                        </li>
                    </ul>
                </li>

            </ul>-->

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->