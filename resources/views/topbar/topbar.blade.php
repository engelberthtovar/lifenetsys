<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" class="rounded-circle">
                <span class="pro-user-name ml-1">
                {{ Auth::user()->name }} . <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0 text-white">
                    {{ __('Administrar Cuenta') }}
                    </h5>
                </div>

                <!-- item-->
                <a href="/user/profile" class="dropdown-item notify-item">
                    <i class="fe-user"></i>
                    <span>{{ __('Mi Perfil') }}</span>
                </a>

                <!-- item-->
                @role('super_admin')
                @if (Route::has('register'))
                <a href="{{ route('register') }}" class="dropdown-item notify-item">
                    <i class="mdi mdi-contact-mail"></i>
                    <span>{{ __('Registrar Usuario') }}</span>
                </a>
                @endif
                @endrole

                <!-- item-->
                <a href="{{ route('invitation') }}" class="dropdown-item notify-item">
                    <i class="mdi mdi-email-plus"></i>
                    <span>{{ __('Enviar invitaciones') }}</span>
                </a>
                @role('super_admin')
                <a href="{{ route('View.Roles') }}" class="dropdown-item notify-item">
                    <i class="mdi mdi-database-plus"></i>
                    <span>{{ __('Crear Role') }}</span>
                </a>

                <a href="{{ route('View.Permissions') }}" class="dropdown-item notify-item">
                    <i class="mdi mdi-eye-plus"></i>
                    <span>{{ __('Crear Permiso') }}</span>
                </a>
                @endrole
            <div class="dropdown-divider"></div>

                <!-- item-->
                    <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a href="{{ route('logout') }}" class="dropdown-item notify-item" 
                onclick="event.preventDefault(); 
                this.closest('form').submit();">
                    <i class="fe-log-out"></i>
                    <span>Cerrar sesión</span>
                </a>
                </form>
        </div>
        </li>

    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="index.html" class="logo text-center">
            <span class="logo-lg">
                <!-- <img src="assets/images/leaf.png" alt="" height="40">-->
                <span><img src="assets/images/logo3.png" alt="" height="50"></span>
                <!-- <span class="logo-lg-text-light">Xeria</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">X</span> -->
                <img src="assets/images/lifeconnections.png" alt="" height="30">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile disable-btn  waves-effect waves-light">

                <span></span>
                <span></span>
                <span></span>
            </button>
        </li>
    </ul>
</div>