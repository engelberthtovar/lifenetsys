        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

        <!-- Plugins js -->
       <!--  <script src="{{ asset('assets/libs/dropzone/dropzone.min.js') }}"></script>-->
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.js"></script>
        

        <!-- Init js-->
        <script src="{{ asset('assets/js/pages/form-fileuploads.init.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>
        
    </body>
</html>