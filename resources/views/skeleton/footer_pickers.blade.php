<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                2020 &copy; Mimirwell <a href="">&nbsp;&hearts;</a> 
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    <a href="javascript:void(0);">About Us</a>
                    <a href="javascript:void(0);">Help</a>
                    <a href="javascript:void(0);">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Vendor js -->
<script src="{{ asset('assets/js/vendor.min.js') }}"></script>

<!-- Third Party js-->
<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>



<!--<script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>-->
<!--<script src="https://apexcharts.com/samples/assets/series1000.js"></script>-->


<script src="{{ asset('assets/libs/jquery-knob/jquery.knob.min.js') }}"></script>
        <!--<script src="assets/libs/peity/jquery.peity.min.js"></script>-->
        




<script src="https://apexcharts.com/samples/assets/ohlc.js"></script>


<!-- datapickers -->
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/clockpicker/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>

<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-pickers.init.js') }}"></script>

<!-- Third widgets JS -->
<script src="{{ asset('assets/libs/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('assets/libs/peity/jquery.peity.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<!-- init js -->
 <script src="{{ asset('assets/js/charts/admindashboard.js') }}"></script>
 <script src="{{ asset('assets/js/pages/dashboard-2.init.js') }}"></script>

<!-- Form Advanced -->
<script src="assets/libs/jquery-nice-select/jquery.nice-select.min.js"></script>
<script src="assets/libs/switchery/switchery.min.js"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/customtreeview.js') }}"></script>
<script src="{{ asset('assets/libs/input-file/input-file.js') }}"></script>
</body>
</html>