<footer class="footer footer-alt">
            2020 &copy; Mimirwell <a href="" class="text-muted"></a> 
        </footer>

        <!-- Vendor js -->
        <script src="assets/js/vendor.min.js"></script>

        <!-- App js -->
        <script src="assets/js/app.min.js"></script>
    </body>
</html>