@include('skeleton.login_header')
<x-jet-validation-errors class="mb-4" />

@if (session('status'))
    <div class="mb-4 font-medium text-sm text-green-600">
        {{ session('status') }}
    </div>
@endif
    <body class="authentication-bg authentication-bg-pattern">
        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <!--<span><img src="assets/images/leaf.png" alt="" height="50"></span>-->
                                        <span><img src="assets/images/lifeconnections.png" alt="" height="130"></span>
                                    </a>
                                    <p class="text-muted mb-4 mt-3">Ingrese su dirección de correo electrónico y contraseña para acceder al panel.</p>
                                </div>

                                <h5 class="auth-title">Sign In</h5>

                                <form method="POST" action="{{ route('login') }}">
                                @csrf
                                    <div class="form-group mb-3">
                                        <label for="emailaddress">Email address</label>
                                        <input class="form-control" type="email" id="emailaddress" placeholder="Enter your email name="email" :value="old('email')" required autofocus">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" id="password" placeholder="Enter your password"  name="password" required autocomplete="current-password">
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox checkbox-info">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-signin">
                                            <label class="custom-control-label" for="checkbox-signin">{{ __('Remember me') }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                    <x-jet-button class="btn btn-danger btn-block">
                                     {{ __('Login In') }}
                                    </x-jet-button>
                                    </div>
                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                            @if (Route::has('password.request'))
                                <p> 
                                    <a class="text-muted ml-1" href="{{ route('password.request') }}">
                                    {{ __('¿Olvidaste tu contraseña?') }}
                                    </a>
                                </p>
                            @endif
                                <!--<p class="text-muted">Don't have an account? <a href="pages-register.html" class="text-muted ml-1"><b class="font-weight-semibold">Sign Up</b></a></p>-->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
@include('skeleton.login_footer')