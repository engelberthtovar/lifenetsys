@if (session('success'))
<div class="alert alert-success" role="alert">
    <button style=" color:gray;"type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&nbsp;&nbsp;&times;</span>
    </button>
    <i class="mdi mdi-check-all mr-2"></i> <strong>Mensaje: &nbsp;</strong> {{ session('success') }} !
</div>
@endif