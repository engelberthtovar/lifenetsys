@if(session('errors'))
<div class="alert alert-danger" role="alert">
    <button style=" color:gray;"type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&nbsp;&nbsp;&times;</span>
    </button>
    <i class="mdi mdi-close-circle-outline mr-2"></i> <strong>Mensaje: &nbsp;</strong> {{ session('errors') }} !
</div>
@endif